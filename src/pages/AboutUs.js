import React, { Component } from 'react';

import { Card } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
//import { styles } from '../theme/styles';
import { Box, Grid } from '@material-ui/core';
import TrendingDownIcon from '@material-ui/icons/TrendingDown';

import ThemeContext from '../context/ThemeContext';
import { Typography, IconButton, Icon } from '@material-ui/core';

import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import ImgBain from '../media/bain.jpg';
import ImgPain from '../media/baguette.jpg';
import ImgAmpoule from '../media/ampoule.jpg';
import logo from '../media/Logogreen.png';

import Fade from '@material-ui/core/Fade';
import Zoom from '@material-ui/core/Zoom';

import vache from '../media/kgboeuf=15000.jpg';
import baguette from '../media/baguette=baignoire.jpg';
import eating from '../media/eating.png';
import scan from '../media/scan.png';
import phone from '../media/phone.png';
const styles = theme => ({
  // root: {
  //   //padding: theme.spacing(6),
  //   backgroundColor: 'white',
  //   display: 'flex',
  //   justifyContent: 'center',
  //   alignItems: 'center',
  //   flexDirection: 'column',
  // },
  /*image: {
    backgroundSize: 'cover',
    backgroundPosition: 'top center',
  }*/
});

class AboutUs extends Component {
  static contextType = ThemeContext;
  constructor(props) {
    super(props);
    this.state = {};
    this.inputReference = React.createRef();
  }

  render() {
    const { classes } = this.props;
    return (
      <div>
      <div style={{...styleFlo.container}}>
        <div style={styleFlo.card}>
          <div style={{...styleFlo.title, paddingTop :0}}>Pourquoi WastePoint ?</div>
          <div style={{...styleFlo.ltitle, paddingTop :50}}>Dans les cantines en France, en moyenne <b>30 à 40% de la nourriture servie est jetée</b>. Lorsqu’il s’agit du gaspillage alimentaire, les chiffres parlent d’eux-mêmes.</div>
          <br/>
          <Grid
                container
                direction="row"
                justify="center"
                alignItems="flex-start"
                style={{flexGrow: 1}}
                spacing={3}
          >
            <Grid item xs={4}>
                <img style={styleFlo.grid.photo} src={baguette}/>
                {/* <div style={styleFlo.grid.writtenby}>{'Rechercher/Modifier un plat'}</div> */}
            </Grid>
            <Grid item xs={4}>
                <img style={styleFlo.grid.photo} src={vache}/>
                {/* <div style={styleFlo.grid.writtenby}>{'Ajouter un plat'}</div> */}
            </Grid>
          </Grid>
          <br/>
          <div style={{...styleFlo.ltitle, paddingTop: 10}}>
          Le gaspillage alimentaire constitue un problème à la fois éthique, moral, social, environnemental et économique.

          <div style={{...styleFlo.title, paddingTop: 90}}> Notre solution</div>
          <br/>
          <br/>
          Pour diminuer le gaspillage, nous devons comprendre sa <b>cause</b> et pouvoir le <b>quantifier</b>. Portion trop importante ? Nourriture pas à notre goût ?
          En réglant le problème à sa source, nous pouvons ainsi simplement <b>produire moins de déchets</b>.
          <br/><br/><br/>
          <Grid
                container
                direction="row"
                justify="center"
                alignItems="flex-start"
                style={{flexGrow: 1}}
                spacing={3}
          >
            <Grid item xs={4}>
                <img style={styleFlo.grid.photo} src={eating}/>
                <div style={styleFlo.grid.writtenby}>{'Inclure le consommateur'}</div>
            </Grid>
            <Grid item xs={4}>
                <img style={styleFlo.grid.photo} src={scan}/>
                <div style={styleFlo.grid.writtenby}>{'Quantifier le gaspillage'}</div>
            </Grid>
            <Grid item xs={4}>
                <img style={styleFlo.grid.photo} src={phone}/>
                <div style={styleFlo.grid.writtenby}>{'Demander des retours'}</div>
            </Grid>
          </Grid>
          <br/><br/>
          Mieux <b>identifier les besoins des consommateurs</b> et informer le plus grand nombre sur les{' '}
                <b>conséquences de notre gaspillage</b> sur la planète est notre objectif. <br/>
                <br />
                WastePoint est donc un outil pour pouvoir comprendre le gaspillage et le diminuer, tant à l'
                <b>échelle individuelle</b> qu'à plusieurs centaines d'
                <b>individus dans un établissement</b>. <br />
                <br />
                Il permet à la cantine de savoir si les <b>portions sont trop grandes</b> ou si des aliments sont{' '}
                <b>systématiquement jetés.</b>
                <br />
                <br />
                Les <b>consommateurs</b>, eux, acquièrent un certain <b>contrôle</b> sur ce qui est proposé et peuvent{' '}
                <b>donner des avis</b> constructifs sur des plats, ce qui peut expliquer les causes du gaspillage.
                <br />
                <br />
                Avec ces clefs en main, nous pouvons rectifier le tir et préserver la planète tout en faisant des{' '}
                <b>économies</b>!
          <div style={{...styleFlo.title, paddingTop: 100}}> Notre équipe</div>
          <br/>
          <br/>
          </div>
          <div style={{...styleFlo.ltitle, paddingTop: 10}}>
          
          <Grid
                container
                direction="row"
                justify="center"
                alignItems="flex-start"
                style={{flexGrow: 1}}
                spacing={6}
          >
            <Grid item xs={2}>
                  <Card>Grégory</Card>
                <div style={styleFlo.grid.writtenby}>{'CEO / Computer Vision'}</div>
            </Grid>
            <Grid item xs={2}>
                  <Card>Florian</Card>
                <div style={styleFlo.grid.writtenby}>{'COO / Backend dev'}</div>
            </Grid>
            <Grid item xs={2}>
                  <Card>Pierre</Card>
                <div style={styleFlo.grid.writtenby}>{'CTO / Backend dev'}</div>
            </Grid>
            <Grid item xs={2}>
                  <Card>Hugo</Card>
                <div style={styleFlo.grid.writtenby}>{'Computer Vision'}</div>
            </Grid>
            <Grid item xs={2}>
                  <Card>Baudouin</Card>
                <div style={styleFlo.grid.writtenby}>{'Mobile dev'}</div>
            </Grid>
            <Grid item xs={2}>
                  <Card>Tiphaine</Card>
                <div style={styleFlo.grid.writtenby}>{'Stratégie commerciale / Web dev'}</div>
            </Grid>
          </Grid>
          <br/>
          <br/>
          <br/>
          </div>
        </div>
      </div>
      </div>
    );
  }
}

export default withStyles(styles)(AboutUs);


var styleFlo = {
  hr: {
    color: "grey"
  },
  grid: {
    title: {
      paddingTop: 15,
      fontSize: 23,
      fontWeight: 600,
      textAlign: "left",
      height: 140
    },
    tag: {
      paddingTop: 30,
      color: "green",
      fontSize: 15,
      fontWeight: 600,
      textAlign: "center"
    },
    writtenby: {
      paddingTop: 20,
      fontSize: 15,
      fontWeight: 400,
      textAlign: "center"
    },
    photo: {
      paddingTop: 10,
      width: 400,
      height: 300
    },
    like: {
      textAlign: "right",
      fontWeight: 800
    
    }
  },
  already: {
    display: "flex",
    marginTop: "50px",
    justifyContent: "center",
    width: "100%",
    alignItems: "center"
  },
  containerTextField: {
    justifyContent: "center",
    alignItems: "center",
    display: "flex",
    width: "100%",
  },
  textfield: {
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
  },
  containerImage: {
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 20,
    width: "100%",
  },
  title: {
    fontSize: 30,
    fontWeight: 400,
    textAlign: "center",
    color: "grey"
  },
  ltitle: {
    fontSize: 20,
    fontWeight: 300,
    textAlign: "center"
  },
  logo: {
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    paddingtop: 30,
    width: "50px",
    height: "50px",
    textAlign: "center",
    justifyContent: "center",
    alignItems: "center"
  },
  container: {
    width: "100%",
    height: "100%",
    paddingTop: 100,
    backgroundRepeat  : 'no-repeat',
    backgroundPosition: 'center',
    backgroundSize: 'cover', 
    display: "flex",
    justifyContent: "center",
  },
  card: {
    width: "70%",
    height: "100%",
    backgroundColor: "white",
    borderRadius: 5,
    justifyContent: "center",
    alignItems: "center",
    //boxShadow: "2px 2px 20px 2px black"

  },
  text: {
    fontSize: 30,
    textAlign: "center",
    fontWeight: 400,
    paddingTop: 50
  },
  button: {
    backgroundColor: "red",
    fontSize: "20px",
    marginRight: 20,
    color: "white"
  },
  secondButton: {
    border: "2px solid black",
    fontSize: "10px",
    marginLeft: 20
  },
  containerButton: {
    justifyContent: "center",
    alignItems: "center",
    display: "flex",
    marginTop: 150
  }
};
