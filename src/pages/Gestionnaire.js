import React, { Component } from 'react';
import ChangePDJ from '../components/Tools/ChangePDJ';
import Add from '../components/Tools/Add';
import SearchBar from '../components/Tools/SearchBar';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import ThemeContext from '../context/ThemeContext';

import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';

import meal from '../media/meal.png';
import menu_meal from '../media/menu_meal.png';
import search_food from '../media/search_food.png';

const styles = theme => ({
  root: {
    backgroundColor: 'white',
    display: 'flex',
    flexDirection: 'column',
  },
  image: {
    backgroundSize: 'cover'
  },
});

class Gestionnaire extends Component {
  static contextType = ThemeContext;
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      open1: false,
      open2: false,
      mot: '',
    };
  }

  validateNew() {
    this.setState({ contact: this.state.mot });
    this.setState({ mot: '', open: false });
  }

  validateNewMenu() {
    this.setState({ contact: this.state.mot });
    this.setState({ mot: '', open1: false });
  }

  checkPlate() {
    this.setState({ contact: this.state.mot });
    this.setState({ mot: '', open2: false });
  }

  render() {
    const { classes } = this.props;
    return (
      <div style={{...styleFlo.container}}>
        <div style={styleFlo.card}>
          <div style={{...styleFlo.ltitle, paddingTop: 10}}>
            Depuis cette page, vous pouvez gérer la liste de plats et le menu du jour !
            <br/><br/><br/>
          </div>
          <div style={{...styleFlo.ltitle, paddingTop: 10}}>
          
            <Grid
                container
                direction="row"
                justify="center"
                alignItems="flex-start"
                style={{flexGrow: 1}}
                spacing={3}
            >
            <Grid item xs={4}>
                <Button>
                <img onClick={() => {this.setState({open2: true})}} style={styleFlo.grid.photo} src={search_food}/>
                </Button>
                <div style={styleFlo.grid.writtenby}>{'Rechercher/Modifier un plat'}</div>
                </Grid>
                <Grid item xs={4}>
                <Button>
                <img onClick={() => {this.setState({open: true})}} style={styleFlo.grid.photo} src={meal}/>
                </Button>
                <div style={styleFlo.grid.writtenby}>{'Ajouter un plat'}</div>
                </Grid>

                <Grid item xs={4}>
                <Button>
                <img onClick={() => {this.setState({open1: true})}} style={styleFlo.grid.photo} src={menu_meal}/>
                </Button>
                <div style={styleFlo.grid.writtenby}>{'Ajouter/Modifier le menu du jour'}</div>
                </Grid>
            </Grid>
          </div>
        </div>
        <Dialog
        open={this.state.open2}
        maxWidth={'sm'}
        fullWidth={true}
      >
        <DialogTitle>
          Rechercher un plat:
        </DialogTitle>
        <DialogContent>
          <SearchBar/>
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={() => {this.setState({open2: false})}} color="primary">
            Annuler
          </Button>
          <Button onClick={() => this.checkPlate()} color="primary">
            OK
          </Button>
        </DialogActions>
      </Dialog>
        <Dialog
        open={this.state.open}
        maxWidth={'sm'}
        fullWidth={true}
      >
        <DialogTitle>
          Ajouter un plat:
        </DialogTitle>
        <DialogContent>
          <Add/>
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={() => {this.setState({open: false})}} color="primary">
            Annuler
          </Button>
          <Button onClick={() => this.validateNew()} color="primary">
            OK
          </Button>
        </DialogActions>
      </Dialog>
      <Dialog
        open={this.state.open1}
        maxWidth={'sm'}
        fullWidth={true}
      >
      <DialogActions>
          <Button autoFocus onClick={() => {this.setState({open1: false})}} color="primary">
            X
          </Button>
        </DialogActions>
        <DialogTitle>
          Sélectionnez les plats du jour:
        </DialogTitle>
        <DialogContent>
          <ChangePDJ/>
        </DialogContent>
      </Dialog>
      </div>
    );
  }
}

export default withStyles(styles)(Gestionnaire);

var styleFlo = {
  hr: {
    color: "grey"
  },
  grid: {
    title: {
      paddingTop: 15,
      fontSize: 23,
      fontWeight: 600,
      textAlign: "left",
      height: 140
    },
    tag: {
      paddingTop: 30,
      color: "green",
      fontSize: 15,
      fontWeight: 600,
      textAlign: "center"
    },
    writtenby: {
      paddingTop: 20,
      fontSize: 15,
      fontWeight: 400,
      textAlign: "center"
    },
    photo: {
      paddingTop: 0,
      width: 200,
      height: 200
    },
    like: {
      textAlign: "right",
      fontWeight: 800
    
    }
  },
  already: {
    display: "flex",
    marginTop: "50px",
    justifyContent: "center",
    width: "100%",
    alignItems: "center"
  },
  containerTextField: {
    justifyContent: "center",
    alignItems: "center",
    display: "flex",
    width: "100%",
  },
  textfield: {
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
  },
  containerImage: {
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 20,
    width: "100%",
  },
  title: {
    fontSize: 30,
    fontWeight: 600,
    textAlign: "center"
  },
  ltitle: {
    fontSize: 20,
    fontWeight: 300,
    textAlign: "center"
  },
  logo: {
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    paddingtop: 30,
    width: "50px",
    height: "50px",
    textAlign: "center",
    justifyContent: "center",
    alignItems: "center"
  },
  container: {
    width: "100%",
    height: "100%",
    paddingTop: 100,
    backgroundRepeat  : 'no-repeat',
    backgroundPosition: 'center',
    backgroundSize: 'cover', 
    display: "flex",
    justifyContent: "center",
  },
  card: {
    width: "70%",
    height: "100%",
    backgroundColor: "white",
    borderRadius: 5,
    justifyContent: "center",
    alignItems: "center",
    //boxShadow: "2px 2px 20px 2px black"

  },
  text: {
    fontSize: 30,
    textAlign: "center",
    fontWeight: 400,
    paddingTop: 50
  },
  button: {
    backgroundColor: "red",
    fontSize: "20px",
    marginRight: 20,
    color: "white"
  },
  secondButton: {
    border: "2px solid black",
    fontSize: "10px",
    marginLeft: 20
  },
  containerButton: {
    justifyContent: "center",
    alignItems: "center",
    display: "flex",
    marginTop: 150
  }
};
