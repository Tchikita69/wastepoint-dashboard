import React, { Component } from 'react';
import Questions from 'react-faq-component';
import Link from '@material-ui/core/Link';
import TextField from '@material-ui/core/TextField';
import { Box } from '@material-ui/core';
import Button from '@material-ui/core/Button';

import Slide from '@material-ui/core/Slide';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';

import CssBaseline from '@material-ui/core/CssBaseline';
import { withStyles } from '@material-ui/core/styles';
import { styles } from '../theme/styles';
import ThemeContext from '../context/ThemeContext';
import logo from '../media/Logogreen.png';
import mail from '../media/mail.jpg';
import telephone from '../media/telephone.jpg';
import Zoom from '@material-ui/core/Zoom';
import axios from 'axios';

const scale = 5;
const width = 500;
const height = 500;

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const TransitionZoom = React.forwardRef(function Transition(props, ref) {
  return <Zoom direction="up" ref={ref} {...props} />;
});

class Contact extends Component {
  static contextType = ThemeContext;
  static contextType = ThemeContext;

  state = {
    bug: '',
    sugg: '',
    question: '',
    subject: '',
    mail: '',
    writeQuestion: false,
    showMail: false,
    showNumber: false,
  };
  handleChangeBug = event => {
    this.setState({ bug: event.target.value });
  };
  handleChangeSugg = event => {
    this.setState({ sugg: event.target.value });
  };

  handleChangeQuestion = event => {
    this.setState({ question: event.target.value });
  };
  handleChangeSubject = event => {
    this.setState({ subject: event.target.value });
  };
  handleChangeMail = event => {
    this.setState({ mail: event.target.value });
  };

  closeDialog = () => {
    this.setState({ writeQuestion: false });
  };
  closeDialogMail = () => {
    this.setState({ showMail: false });
  };

  closeDialogNumber = () => {
    this.setState({ showNumber: false });
  };

  openDialog = () => {
    this.setState({ writeQuestion: true });
  };

  openDialogMail = () => {
    this.setState({ showMail: true });
  };

  openDialogNumber = () => {
    this.setState({ showNumber: true });
  };

  sendToWP = () => {
    axios({
      method: 'get',
      url: process.env.REACT_APP_API_URL + '/dashboard/messages/send',
      headers: {
        Authorization: 'Bearer ' + sessionStorage.getItem('token'),
      },
      params: {
        mail: this.state.mail,
        object: this.state.subject,
        message: this.state.question,
      },
    }).then(
      res => {
        console.log(res);
        alert(
          'Merci! Votre question a bien été prise en compte. Vous serez notifié de la réponse de notre équipe dans les plus brefs délais.'
        );
      },
      err => {
        console.log(err);
        alert('Désolé, cette action a échoué.');
      }
    );
    this.setState({ writeQuestion: false });
  };
  render() {
    const { classes } = this.props;
    return (
      <div style={{marginLeft: 50}} className={classes.root}>
        <CssBaseline />
        <Box display="flex" width={1600} height={300} marginTop={10} alignItems="center" justifyContent="center">
          <Box m="auto">
            <h1
              style={{
                marginLeft: 150,
                color: this.context.style.buttonBackground,
                fontSize: 20,
                fontWeight: 'normal',
              }}
            >
              Si vous avez une question sans réponse dans la{' '}
              <Link to="/faq" style={{ fontWeight: 20, fontSize: 25 }} color="green">
                FAQ
              </Link>
            </h1>
            <h1
              style={{
                marginLeft: 200,
                color: this.context.style.buttonBackground,
                fontSize: 20,
                fontWeight: 'normal',
              }}
            >
              Si vous avez trouvé un dysfonctionnement
            </h1>
            <h1
              style={{
                marginLeft: 230,
                color: this.context.style.buttonBackground,
                fontSize: 20,
                fontWeight: 'normal',
              }}
            >
              Si vous vous voulez nous rencontrer
            </h1>
            <Zoom in={true} style={{ transitionDelay: '200ms' }}>
              <img
                style={{
                  marginLeft: 370,
                  marginTop: 50,
                  marginBottom: 20,
                  borderRadius: 15,
                }}
                src={logo}
                width={width / scale}
                height={height / scale}
                alt="Logo"
              />
            </Zoom>
            <h1
              style={{
                marginLeft: 140,
                color: this.context.style.buttonBackground,
                fontSize: 30,
                fontWeight: 'normal',
              }}
            >
              N'hésitez pas à nous contacter{' '}
              <Link
                onClick={() => this.openDialog()}
                style={{
                  textDecorationLine: 'underline',
                  color: this.context.style.buttonBackground,
                }}
              >
                ICI
              </Link>{' '}
              ou:
            </h1>
          </Box>
        </Box>
        <Box display="flex" width={1750} height={270} marginTop={10} alignItems="center" justifyContent="center">
          <Zoom in={true} style={{ transitionDelay: '1000ms' }}>
            <img
              style={{
                marginLeft: 0,
                marginTop: 50,
                marginBottom: 20,
                borderRadius: 20,
              }}
              src={mail}
              width={(width / scale) * 1.25}
              height={(height / scale) * 1.25}
              alt="mail"
              onClick={() => this.openDialogMail()}
            />
          </Zoom>
          <Zoom in={true} style={{ transitionDelay: '1100ms' }}>
            <img
              style={{
                marginLeft: 60,
                marginTop: 50,
                marginBottom: 20,
                borderRadius: 20
              }}
              src={telephone}
              width={(width / scale) * 1.2}
              height={(height / scale) * 1.2}
              alt="telephone"
              onClick={() => this.openDialogNumber()}
            />
          </Zoom>
        </Box>
        <Dialog
          open={this.state.writeQuestion}
          onClose={() => this.closeDialog()}
          TransitionComponent={Transition}
        >
          <DialogTitle>Bonjour !</DialogTitle>
          <DialogContent>
            <DialogContentText>
              <TextField
                InputProps={{ className: classes.input }}
                margin="normal"
                id="mail"
                label="Votre adresse e-mail"
                onChange={this.handleChangeMail}
                style={{ backgroundColor: 'white' }}
              />
              <br />
              <TextField
                InputProps={{ className: classes.input }}
                id="subject"
                label="Sujet"
                onChange={this.handleChangeSubject}
                style={{ backgroundColor: 'white' }}
              />
              <br />
              <TextField
                InputProps={{ className: classes.input }}
                margin="normal"
                id="question"
                label="Votre texte ici"
                multiline
                rows={12}
                variant="outlined"
                onChange={this.handleChangeQuestion}
                style={{ backgroundColor: 'white', width: 500 }}
              />
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button autoFocus onClick={() => this.closeDialog()} color="primary">
              Annuler
            </Button>
            <Button onClick={this.sendToWP} color="primary">
              Envoyer
            </Button>
          </DialogActions>
        </Dialog>
        <Dialog
          open={this.state.showMail}
          onClose={() => this.closeDialogMail()}
          maxWidth={'sm'}
          fullWidth={true}
          TransitionComponent={TransitionZoom}
        >
          <DialogContent>
            <DialogContentText style={{ fontSize: 40 }}>wastepoint.contact@gmail.com</DialogContentText>
          </DialogContent>
        </Dialog>
        <Dialog
          open={this.state.showNumber}
          onClose={() => this.closeDialogNumber()}
          maxWidth={'sm'}
          fullWidth={true}
          TransitionComponent={TransitionZoom}
        >
          <DialogContent>
            <DialogContentText style={{ fontSize: 63 }}>+33 6 61 54 25 70</DialogContentText>
          </DialogContent>
        </Dialog>
        <Box display="flex" width={1750} height={10} marginTop={10} alignItems="center" justifyContent="center"></Box>
      </div>
    );
  }
}

export default withStyles(styles)(Contact);
