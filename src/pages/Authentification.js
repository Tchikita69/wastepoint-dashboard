import React, { Component } from 'react';
import { Paper, Grid, TextField, Button, FormControlLabel, Checkbox } from '@material-ui/core';
import { PermIdentity, Lock } from '@material-ui/icons';
import { AuthContext } from '../context/auth';
import '../styles/auth.css';
import FirstConnection from './FirstConnection';
import axios from 'axios';

import { Redirect } from 'react-router-dom';

import InputAdornment from '@material-ui/core/InputAdornment';

// MEDIA
import logo from '../media/Wastepointtextwhite.png';

const classes = {
  margin: {
    margin: '40px',
  },
  padding: {
    padding: '20px',
    backgroundColor: 'white',
    borderRadius: 10,
    width: 500,
  },
};

const scale = 5;
const width = 1539;
const height = 477;

export default class Authentification extends Component {
  static contextType = AuthContext;

  constructor(props) {
    super(props);
    this.state = {
      user_id: '',
      password: '',
      loggedIn: false,
      first_co: false,
    };
    this._handleUsernameChange = this._handleUsernameChange.bind(this);
    this._handlePasswordChange = this._handlePasswordChange.bind(this);
  }

  _handleUsernameChange(e) {
    this.setState({ user_id: e.target.value });
  }

  _handlePasswordChange(e) {
    this.setState({ password: e.target.value });
  }

  checkComplexityMdp() {
      var upper = false;
      var numbers = false;
      var special = false;
      var length8 = false;
      var pwd = this.state.password;

      var matchedCase = [];
      matchedCase.push('[$@$!%*#?&]'); // Special Charector
      matchedCase.push('[A-Z]'); // Uppercase Alpabates
      matchedCase.push('[0-9]'); // Numbers
      matchedCase.push('[a-z]'); // Lowercase Alphabates
  
      length8 = pwd.length >= 8 ? true : false;
      upper = new RegExp(matchedCase[1]).test(pwd) && new RegExp(matchedCase[3]).test(pwd) ? true : false;
      numbers = new RegExp(matchedCase[2]).test(pwd) ? true : false;
      special = new RegExp(matchedCase[0]).test(pwd) ? true : false;

      if (upper && numbers && special && length8) {
        return (true);
      }
      return (false);
  }

  authRequest() {
    if (this.state.user_id == '' || this.state.password == '') this.setState({ first_co: false });
    else {
      console.log(process.env.REACT_APP_API_URL + '/dashboard/users/login/')
      axios({
        method: 'GET',
        url: process.env.REACT_APP_API_URL + '/dashboard/users/login/',
        params: {
          username: this.state.user_id,
          password: this.state.password,
        },
      })
        .then(response => {
          if (response.data.token) {
            sessionStorage.setItem('token', response.data.token);
            sessionStorage.setItem('user_id', this.state.user_id);
            if (this.checkComplexityMdp() == false)
              this.setState({ first_co: true });
            else {
              this.context.setAuthTokens(this.state.user_id + this.state.password);
              this.setState({loggedIn: true});
            }
          } else {
            alert("Erreur d'authentification, veuillez réessayer. Vérifiez que vos identifiants sont corrects.");
          }
        })
        .catch(error => {
          alert("Erreur d'authentification, veuillez réessayer. Vérifiez que vos identifiants sont corrects.");
          console.log(error);
        });
    }
  }

  render() {
    if (this.state.first_co) return <FirstConnection />;
    else if (this.state.loggedIn) return <Redirect to="/home" />;
    return (
      <div className="content">
        <Grid
          container
          spacing={0}
          direction="column"
          alignItems="center"
          justify="center"
          style={{ minHeight: '100vh' }}
        >
          <Paper style={classes.padding} elevation={4}>
            <div style={classes.margin}>
              <Grid container alignItems="flex-end" justify="center">
                <img
                  style={{ marginBottom: '50px' }}
                  src={logo}
                  width={width / scale}
                  height={height / scale}
                  alt="Logo"
                />
              </Grid>
              <Grid container spacing={3} alignItems="flex-end" justify="center">
                <Grid item>
                  <TextField
                    variant="outlined"
                    id="user_id"
                    label="Identifiant"
                    fullWidth
                    required
                    value={this.state.user_id}
                    onChange={this._handleUsernameChange}
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <PermIdentity />
                        </InputAdornment>
                      ),
                    }}
                  />
                </Grid>
              </Grid>
              <Grid container spacing={3} alignItems="flex-end" justify="center">
                <Grid item>
                  <TextField
                    variant="outlined"
                    id="password"
                    label="Mot de passe"
                    type="password"
                    fullWidth
                    required
                    value={this.state.password}
                    onChange={this._handlePasswordChange}
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <Lock />
                        </InputAdornment>
                      ),
                    }}
                  />
                </Grid>
              </Grid>
              <Grid container alignItems="center" justify="space-between" style={{ marginTop: '30px' }}>
                <Grid item>
                  <FormControlLabel control={<Checkbox color="primary" />} label="Se rappeler de moi" />
                </Grid>
              </Grid>
              <Grid container justify="center" style={{ marginTop: '20px' }}>
                <button
                  id="connection_btn"
                  onClick={() => this.authRequest()}
                  variant="outlined"
                  color="primary"
                  style={{ textTransform: 'none' }}
                >
                  Se connecter
                </button>
              </Grid>
            </div>
          </Paper>
        </Grid>
      </div>
    );
  }
}
