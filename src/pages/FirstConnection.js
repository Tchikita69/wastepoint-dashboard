import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';

import { withStyles } from '@material-ui/core/styles';

import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import MuiDialogActions from '@material-ui/core/DialogActions';

import CheckIcon from '@material-ui/icons/Check';
import ClearIcon from '@material-ui/icons/Clear';

import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import passwordStrength from 'check-password-strength';
import { AuthContext } from '../context/auth';

import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

import Link from '@material-ui/core/Link';
import axios from 'axios';

toast.configure();

const styles = theme => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
    width: 550,
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});

const DialogContent = withStyles(theme => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

const DialogActions = withStyles(theme => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

const DialogTitle = withStyles(styles)(props => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography style={{ align: 'center' }} variant="h6">
        {children}
      </Typography>
    </MuiDialogTitle>
  );
});

export default class FirstConnection extends Component {
  static contextType = AuthContext;

  constructor(props) {
    super(props);
    this.state = {
      loggedIn: false,
      open_state: true,
      password: '',
      strength: 'Faible',
      password2: '',
      visibility: false,
      length8: false,
      upper: false,
      numbers: false,
      special: false,
    };
    this.closewindow = this.closewindow.bind(this);
    this._handlePasswordChange = this._handlePasswordChange.bind(this);
    this._handlePassword2Change = this._handlePassword2Change.bind(this);
  }

  to_fr(complex) {
    switch (complex) {
      case 'Weak':
        return 'Faible';
      case 'Medium':
        return 'Moyen';
      case 'Strong':
        return 'Fort';
      default:
        return 'Faible';
    }
  }

  _handlePasswordChange(e) {
    this.setState({ password: e.target.value });
    this.checkconditions(e.target.value);
    this.setState({
      strength: e.target.value !== '' ? this.to_fr(passwordStrength(e.target.value).value) : 'Faible',
    });
  }

  checkconditions(pwd) {
    var matchedCase = [];
    matchedCase.push('[$@$!%*#?&]'); // Special Charector
    matchedCase.push('[A-Z]'); // Uppercase Alpabates
    matchedCase.push('[0-9]'); // Numbers
    matchedCase.push('[a-z]'); // Lowercase Alphabates

    this.setState({ length8: pwd.length >= 8 ? true : false });
    this.setState({
      upper: new RegExp(matchedCase[1]).test(pwd) && new RegExp(matchedCase[3]).test(pwd) ? true : false,
    });
    this.setState({
      numbers: new RegExp(matchedCase[2]).test(pwd) ? true : false,
    });
    this.setState({
      special: new RegExp(matchedCase[0]).test(pwd) ? true : false,
    });
  }

  _handlePassword2Change(e) {
    this.setState({ password2: e.target.value });
  }

  closewindow() {
    console.log('closed window');
  }

  checkchar() {
    var matchedCase = [];
    var message;
    matchedCase.push('[$@$!%*#?&]'); // Special Charector
    matchedCase.push('[A-Z]'); // Uppercase Alpabates
    matchedCase.push('[0-9]'); // Numbers
    matchedCase.push('[a-z]'); // Lowercase Alphabates
    var test = 0;

    for (var i = 0; i < matchedCase.length; i++) {
      switch (i) {
        case 0:
          message = 'Le mot de passe doit contenir au moins un caractère spécial';
          break;
        case 1:
          message = 'Le mot de passe doit contenir au moins une majuscule';
          break;
        case 2:
          message = 'Le mot de passe doit contenir au moins un chiffre';
          break;
        case 3:
          message = 'Le mot de passe doit contenir au moins une minuscule';
          break;
        default:
          break;
      }
      if (!new RegExp(matchedCase[i]).test(this.state.password)) {
        toast.error(message, {
          hideProgressBar: true,
          position: 'bottom-right',
        });
      } else {
        test += 1;
      }
    }
    return test === 4 ? true : false;
  }

  pass_valid() {
    if (this.state.password.length < 8) {
      toast.error(
        <div style={{ color: 'black', margin: 20 }}>
          <h4>Erreur: </h4>Le mot de passe doit faire au moins 8 caractères.
        </div>,
        { hideProgressBar: true, position: 'bottom-right' }
      );
      return false;
    }
    if (this.state.password !== this.state.password2) {
      toast.error(
        <div style={{ color: 'black', margin: 20 }}>
          <h4>Erreur: </h4>Les mots de passe ne correspondent pas.
        </div>,
        { hideProgressBar: true, position: 'bottom-right' }
      );
      return false;
    }
    if (!this.checkchar()) return false;
    return true;
  }

  confirm() {
    var user_id = sessionStorage.getItem('user_id');
    if (this.pass_valid()) {
      this.context.setAuthTokens(this.state.user_id + this.state.password);
      axios({
        method: 'GET',
        url: process.env.REACT_APP_API_URL + '/dashboard/users/updatePass/',
        params: {
          username: user_id,
          password: this.state.password,
        },
      })
        .then(response => {
          alert("Mot de passe modifié !");
          this.setState({ loggedIn: true });
        })
        .catch(error => {
          alert("Erreur. Le mot de passe n'a pas pu être enregistré...");
          console.log(error);
        });
    }
  }

  skipModif() {
    this.context.setAuthTokens(this.state.user_id + this.state.password);
    this.setState({ loggedIn : true });
  }

  render() {
    if (this.state.loggedIn) {
      return <Redirect to="/home" />;
    }
    return (
      <Dialog onClose={this.closewindow} aria-labelledby="customized-dialog-title" open={this.state.open_state}>
        <DialogTitle id="customized-dialog-title" onClose={this.closewindow}>
          Votre mot de passe n'est pas sécurisé
        </DialogTitle>
        <DialogContent dividers>
          <Box m={1}>
            <Typography style={{ fontWeight: 'bold' }} variant="h6">
              Pour des raisons de sécurité, veuillez modifier votre mot de passe:
            </Typography>
          </Box>
          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'column',
            }}
          >
            <Box m={2}>
              <TextField
                type={this.state.visibility ? 'text' : 'password'}
                id="password"
                label="Nouveau mot de passe"
                required
                variant="outlined"
                style={{ width: '350px' }}
                value={this.state.password}
                onChange={this._handlePasswordChange}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton
                        onClick={() =>
                          this.setState({
                            visibility: this.state.visibility ? false : true,
                          })
                        }
                      >
                        {this.state.visibility ? <VisibilityOff /> : <Visibility />}
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
              />
            </Box>
            <Box m={1}>
              <TextField
                variant="outlined"
                type="password"
                id="password2"
                label="Confirmer le nouveau mot de passe"
                style={{ width: '350px' }}
                required
                value={this.state.password2}
                onChange={this._handlePassword2Change}
              />
            </Box>
          </div>
          <Box m={1} marginTop={2}>
            <Typography variant="subtitle2">Complexité du mot de passe: {this.state.strength}</Typography>
          </Box>
          <Box m={1} marginTop={2}>
            <Typography variant="body1">Votre nouveau mot de passse doit:</Typography>
            <Typography style={{ color: this.state.length8 ? 'green' : 'red' }} variant="body2">
              - Faire au minimum 8 caractères{' '}
              {this.state.length8 ? (
                <CheckIcon color="inherit" fontSize="inherit" />
              ) : (
                <ClearIcon color="inherit" fontSize="inherit" />
              )}{' '}
            </Typography>
            <Typography style={{ color: this.state.upper ? 'green' : 'red' }} variant="body2">
              - Contenir des majuscules et minuscules{' '}
              {this.state.upper ? (
                <CheckIcon color="inherit" fontSize="inherit" />
              ) : (
                <ClearIcon color="inherit" fontSize="inherit" />
              )}{' '}
            </Typography>
            <Typography style={{ color: this.state.numbers ? 'green' : 'red' }} variant="body2">
              - Contenir au moins un chiffre{' '}
              {this.state.numbers ? (
                <CheckIcon color="inherit" fontSize="inherit" />
              ) : (
                <ClearIcon color="inherit" fontSize="inherit" />
              )}{' '}
            </Typography>
            <Typography style={{ color: this.state.special ? 'green' : 'red' }} variant="body2">
              - Contenir au moins un caractère spécial{' '}
              {this.state.special ? (
                <CheckIcon color="inherit" fontSize="inherit" />
              ) : (
                <ClearIcon color="inherit" fontSize="inherit" />
              )}{' '}
            </Typography>
          </Box>
        </DialogContent>
        <DialogActions>
        <Box m={1}>
        <Link
              style = {{marginRight: 20, color: "inherit"}}
              onClick={() => {
                this.skipModif();
              }}
            >
              Passer cet étape
            </Link>
            </Box>
          <Box m={1}>
            <Button
              onClick={() => {
                this.confirm();
              }}
              variant="contained"
              color="primary"
            >
              Valider la modification
            </Button>
          </Box>
        </DialogActions>
      </Dialog>
    );
  }
}
