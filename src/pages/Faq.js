import React, { Component } from 'react';
import Questions from 'react-faq-component';
import Link from '@material-ui/core/Link';
import TextField from '@material-ui/core/TextField';
import { Box } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import SentimentVeryDissatisfiedIcon from '@material-ui/icons/SentimentVeryDissatisfied';
import SentimentVerySatisfiedIcon from '@material-ui/icons/SentimentVerySatisfied';
import SentimentVerySatisfiedFull from '@material-ui/icons/SentimentVerySatisfiedTwoTone';
import SentimentVeryDissatisfiedFull from '@material-ui/icons/SentimentDissatisfiedTwoTone';
import SearchIcon from '@material-ui/icons/Search';
import { ButtonGroup, Paper } from '@material-ui/core';
import axios from 'axios';

import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';

import CssBaseline from '@material-ui/core/CssBaseline';
import { withStyles } from '@material-ui/core/styles';
import { styles } from '../theme/styles';
import ThemeContext from '../context/ThemeContext';
import logo from '../media/Logogreen.png';
import Slide from '@material-ui/core/Slide';
import Zoom from '@material-ui/core/Zoom';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="left" ref={ref} {...props} />;
});

const init = {
  title: '',
  rows: [
    {
      title: "Je n'arrive pas à changer mes outils",
      content:
        "Cliquez en haut à gauche sur le bouton 'montrer la barre d'options'. Cliquez sur l'onglet 'Mes outils' pour accéder au menu des outils. D'ici, vous pouvez choisir les outils que vous souhaitez afficher.",
    },
    {
      title: 'Comment contacter WastePoint?',
      content:
        "Cliquez en haut à gauche sur le bouton 'montrer la barre d'options'. Cliquez sur l'onglet 'Contact' pour pouvoir nous joindre par mail ou par téléphone.",
    },
    {
      title: "J'ai trouvé un bug dans l'application. Comment le faire remonter?",
      content: "Vous pouvez nous contacter en cliquant sur l'onglet 'Contact'.",
    },
    {
      title: 'Comment déplacer mes outils?',
      content:
        "Pour déplacer vos outils sur la page principale de gestion, restez appuyé sur un outil et déplacez le jusqu'à la position souhaitée puis relâchez.",
    },
  ],
  styles: {
    bgColor: 'yellow',
  },
};

const scale = 5;
const width = 250;
const height = 250;

const happyEmo = feedback => {
  if (feedback == false || feedback == 'neutral')
    return <SentimentVerySatisfiedIcon style={{ fontSize: 50, fill: 'green' }} />;
  else return <SentimentVerySatisfiedFull style={{ fontSize: 50, fill: 'green' }} />;
};

const badEmo = feedback => {
  if (feedback == true || feedback == 'neutral')
    return <SentimentVeryDissatisfiedIcon style={{ fontSize: 50, fill: 'red' }} />;
  else return <SentimentVeryDissatisfiedFull style={{ fontSize: 50, fill: 'red' }} />;
};

class Faq extends Component {
  static contextType = ThemeContext;
  state = {
    mot: '',
    data: init,
    dataShown: init,
    writeQuestion: false,
    question: '',
    mail: '',
    rows: [],
    satisfy: 'inline',
    nodata: 'inline',
    noresult: 'none',
    feedback: 'neutral',
  };

  componentDidMount() {
    var data = [];
    axios({
      method: 'get',
      url: process.env.REACT_APP_API_URL + '/dashboard/QA/getAll',
      headers: {
        Authorization: 'Bearer ' + sessionStorage.getItem('token'),
      },
    }).then(res => {
      var i = 0;
      var result = [];
      var data2 = {
        title: '',
        rows: [],
        styles: {
          bgColor: 'red',
        },
      };
      data = res.data.faq;
      for (i = 0; i < data.length; i++) {
        var row = { title: '', content: '' };
        row.title = data[i].question;
        row.content = data[i].response;
        data2.rows.push(row);
      }
      this.setState({ data: data2, dataShown: data2 });
    });
  }

  handleChangeMot = event => {
    this.setState({ mot: event.target.value });
  };
  handleChangeQuestion = event => {
    this.setState({ question: event.target.value });
  };
  handleChangeMail = event => {
    this.setState({ mail: event.target.value });
  };

  advice = note => {
    this.setState({ feedback: note });
    axios({
      method: 'post',
      url: process.env.REACT_APP_API_URL + '/dashboard/QA/feedback/post',
      headers: {
        Authorization: 'Bearer ' + sessionStorage.getItem('token'),
      },
      params: {
        feedback: note,
      },
    }).then(
      res => {
        console.log(res);
        if (note == true) alert('Merci, votre avis a été pris en compte!');
        else alert("Désolé que vous n'ayez pas été satisfait :( Vous pouvez nous envoyer un message!");
      },
      err => {
        console.log(err);
        alert('Désolé, cette action a échoué.');
      }
    );
  };

  closeDialog = () => {
    this.setState({ writeQuestion: false });
  };

  openDialog = () => {
    this.setState({ writeQuestion: true });
  };

  filter = () => {
    const mot = this.state.mot.toLowerCase();
    const totalData = this.state.data.rows;

    var rows = [];
    totalData.forEach((question, index) => {
      if (question.title.toLowerCase().includes(mot)) rows.push(question);
    });
    var data = {
      title: '',
      rows: rows,
      styles: {
        bgColor: 'red',
      },
    };
    this.setState({ dataShown: data });
    if (rows.length === 0) this.setState({ nodata: 'none', noresult: 'inline' });
    else this.setState({ nodata: 'inline', noresult: 'none' });
  };
  sendToWP = () => {
    axios({
      method: 'get',
      url: process.env.REACT_APP_API_URL + '/dashboard/messages/send',
      headers: {
        Authorization: 'Bearer ' + sessionStorage.getItem('token'),
      },
      params: {
        mail: this.state.mail,
        object: '[FAQ] Question',
        message: this.state.question,
      },
    }).then(
      res => {
        console.log(res);
        alert(
          'Merci! Votre question a bien été prise en compte. Vous serez notifié de la réponse de notre équipe dans les plus brefs délais.'
        );
        this.setState({ question: '' });
      },
      err => {
        console.log(err);
        alert('Désolé, cette action a échoué.');
      }
    );
    this.setState({ writeQuestion: false });
  };
  render() {
    const { classes } = this.props;
    return (
      <div style={{marginLeft: 50}} className={classes.root}>
        <CssBaseline />
        <Box display="flex" width={1750} height={40} alignItems="center" justifyContent="center">
          <Box m="auto">
            <Zoom in={true} style={{ transitionDelay: '200ms' }}>
              <img
                style={{ marginLeft: 250, marginTop: 50, borderRadius: 15 }}
                src={logo}
                width={width / scale}
                height={height / scale}
                alt="Logo"
              />
            </Zoom>
            <h1
              style={{
                color: this.context.style.buttonBackground,
                fontSize: 30,
                fontWeight: 'normal',
              }}
            >
              Comment pouvons-nous vous aider ?
            </h1>
          </Box>
        </Box>
        <br />
        <br />
        <br />
        <br />
        <Box display="flex" width={1750} height={200} alignItems="center" justifyContent="center">
          <Box m="auto">
            <div style={{ marginLeft: 60 }}>
              <TextField
                InputProps={{ className: classes.input }}
                margin="normal"
                id="mot"
                label="Décrivez votre problème"
                value={this.state.mot}
                onChange={this.handleChangeMot}
                variant="outlined"
                style={{ backgroundColor: 'white' }}
              />
              <Button
                className={classes.submit}
                style={{
                  backgroundColor: this.context.style.buttonBackground,
                  borderRadius: 20,
                  marginLeft: 10,
                }}
                variant="outlined"
                onClick={this.filter}
              >
                <SearchIcon></SearchIcon>
              </Button>
            </div>
          </Box>
        </Box>
        <div style={{ paddinLeft: 5, display: this.state.nodata }}>
          <Box display="flex" width={1750} height={200} alignItems="center" justifyContent="center">
            <Paper
              style={{
                round: '50%',
                width: '50%',
                paddingTop: 10,
                paddingLeft: 30,
                paddingRight: 30,
              }}
            >
              <Questions data={this.state.dataShown} />
            </Paper>
          </Box>
        </div>
        <div style={{ display: this.state.noresult }}>
          <Box display="flex" width={1700} height={100} alignItems="center" justifyContent="center">
            <h2
              style={{
                marginTop: 0,
                color: 'black',
                fontSize: 20,
                fontWeight: 'normal',
              }}
            >
              Aucun résultat
            </h2>
          </Box>
        </div>
        <br />
        <br />
        <br />
        <br />
        <Zoom in={true} style={{ transitionDelay: '2s' }}>
          <Box display="flex" width={1750} height={10} alignItems="center" justifyContent="center">
            <Link>
              <h2
                style={{
                  textDecorationLine: 'underline',
                  fontSize: 20,
                  fontWeight: 'normal',
                  color: this.context.style.buttonBackground,
                }}
                onClick={() => this.openDialog()}
              >
                Une question sans réponse?
              </h2>
            </Link>
          </Box>
        </Zoom>
        <Dialog
          open={this.state.writeQuestion}
          onClose={() => this.closeDialog()}
          TransitionComponent={Transition}
          maxWidth={'sm'}
          fullWidth={true}
        >
          <DialogTitle>Vous pouvez nous envoyer votre question</DialogTitle>
          <DialogContent>
            <DialogContentText>
              <TextField
                InputProps={{ className: classes.input }}
                margin="normal"
                id="question"
                label="Ecrivez votre question..."
                variant="outlined"
                onChange={this.handleChangeQuestion}
                style={{ backgroundColor: 'white' }}
              />
              <br />
              <TextField
                InputProps={{ className: classes.input }}
                margin="normal"
                id="mail"
                label="Votre adresse e-mail"
                variant="outlined"
                onChange={this.handleChangeMail}
                style={{ backgroundColor: 'white' }}
              />
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button autoFocus onClick={() => this.closeDialog()} color="primary">
              Annuler
            </Button>
            <Button onClick={this.sendToWP} color="primary">
              Envoyer
            </Button>
          </DialogActions>
        </Dialog>
        <div style={{ display: this.state.satisfy }}>
          <Box display="flex" width={1750} height={10} marginTop={7} alignItems="center" justifyContent="center">
            <h2 style={{ fontSize: 20, fontWeight: 'normal', color: 'grey' }}>
              Avez-vous été satisfait de notre rubrique "Questions Fréquentes" ?
            </h2>
          </Box>
          <Box display="flex" width={1720} height={143} alignItems="center" justifyContent="center">
            <ButtonGroup>
              <Button
                className={classes.button}
                variant="text"
                onClick={() => {
                  this.advice(true);
                }}
              >
                {happyEmo(this.state.feedback)}
              </Button>
              <Button
                className={classes.testbutton}
                variant="text"
                onClick={() => {
                  this.advice(false);
                }}
              >
                {badEmo(this.state.feedback)}
              </Button>
            </ButtonGroup>
          </Box>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(Faq);
