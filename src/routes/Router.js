import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
// MY COMPONENTS IMPORT
import Authentification from '../pages/Authentification';
import Dashboard from '../components/Dashboard';
import Faq from '../pages/Faq';
import Contact from '../pages/Contact';
import Settings from '../pages/Settings';
import Gestionnaire from '../pages/Gestionnaire';
import AboutUs from '../pages/AboutUs';
//routes
import PrivateRoute from './PrivateRoute';
import Layout from '../components/layout/DefaultLayout';

// ----------------------------------------------------------------------

export default function Router() {
    return (
        <Switch>
            <Route path="/auth" component={Authentification} />
            <PrivateRoute path="/home" component={Dashboard} layout={Layout} />
            <Redirect exact from="/" to="/home" />
            <PrivateRoute path="/mytools" component={Settings} layout={Layout} />
            <PrivateRoute path="/faq" component={Faq} layout={Layout} />
            <PrivateRoute path="/contact" component={Contact} layout={Layout} />
            <PrivateRoute path="/profil" component={Gestionnaire} layout={Layout} />
            <PrivateRoute path="/aboutus" component={AboutUs} layout={Layout} />
        </Switch>
    );
}
