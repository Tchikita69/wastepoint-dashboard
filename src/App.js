import React, { useState, useContext } from 'react';

import Router from './routes/Router.js';

import ScrollToTop from './components/scrollParameters/ScrollToTop';


// ROUTES  and CONTEXT

import { AuthContext } from './context/auth';
import ThemeContext from './context/ThemeContext';
import ThemeConfig from './theme';
// LAYOUTS


function App(props) {
  const existingTokens = JSON.parse(sessionStorage.getItem('tokens'));
  const [authTokens, setAuthTokens] = useState(existingTokens);

  const setTokens = data => {
    sessionStorage.setItem('tokens', JSON.stringify(data));
    setAuthTokens(data);
  };
  if (window.sessionStorage.getItem('theme') == undefined)
    window.sessionStorage.setItem('theme', 'green');
  const { style } = useContext(ThemeContext);

  return (
    <div style={{ backgroundColor: style.appBackground }}>
      <AuthContext.Provider value={{ authTokens, setAuthTokens: setTokens }}>
      <ThemeConfig>
        <ScrollToTop />
        <Router />
      </ThemeConfig>
      </AuthContext.Provider>
    </div>
  );
}

export default App;
