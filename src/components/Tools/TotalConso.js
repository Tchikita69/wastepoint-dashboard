import React, { Component } from 'react';
import { withStyles } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import TrendingUpIcon from '@material-ui/icons/TrendingUp';
import TrendingDownIcon from '@material-ui/icons/TrendingDown';
import axios from 'axios';

const styles = theme => ({
  root: {
    padding: theme.spacing(6),
    backgroundColor: 'white',
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  first: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  second: {
    display: 'flex',
    flexDirection: 'column',
  },
});

class TotalConso extends Component {
  state = {
    nbConso: '0',
    consoAugmPourc: '- 0',
    nbConnexions: '0',
    pourcentConnexion: '0',
    connexAugmPourc: '+ 0',
    nbRetours: '0',
  };

  getNbConso() {
    axios({
      method: 'get',
      url: process.env.REACT_APP_API_URL + '/dashboard/get/trays',
      headers: {
        Authorization: 'Bearer ' + sessionStorage.getItem('token'),
      },
      params: {
        day: '',
        period: '',
      },
    }).then(res => {
      this.setState({ nbConso: res.data.trays });
    });
  }

  getNbConnexions() {
    axios({
      method: 'get',
      url: process.env.REACT_APP_API_URL + '/dashboard/get/connections',
      headers: {
        Authorization: 'Bearer ' + sessionStorage.getItem('token'),
      },
    }).then(res => {
      this.setState({ nbConnexions: res.data.connections });
    });
  }

  componentDidMount() {
    this.getNbConso();
    this.getNbConnexions();
  }

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <div className={classes.first}>
          <div className={classes.second}>
            <Typography gutterBottom variant="subtitle3" display="inline">
              {"Aujourd'hui"}
            </Typography>
            <Typography gutterBottom variant="h5">
              Nombre de consommateurs{' '}
            </Typography>
            <Typography gutterBottom variant="h4" style={{ fontWeight: 'bold' }} display="inline">
              {this.state.nbConso}
              <TrendingDownIcon style={{ paddingTop: 10, fontSize: 40, fill: 'red' }} />
            </Typography>
            <Typography gutterBottom variant="h5">
              Nombre de connexions à l'application mobile{' '}
            </Typography>
            <Typography gutterBottom variant="h4" style={{ fontWeight: 'bold' }} display="inline">
              {this.state.nbConnexions}
              <TrendingUpIcon style={{ paddingTop: 10, fontSize: 40, fill: 'green' }} />
            </Typography>
          </div>
          <TrendingUpIcon style={{ fontSize: 80 }} /> 
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(TotalConso);
