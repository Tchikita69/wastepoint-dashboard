import React, { Component } from 'react';
import { withStyles } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import { Link } from 'react-router-dom';
import axios from 'axios';

import Dialog from '@material-ui/core/Dialog';
import Zoom from '@material-ui/core/Zoom';

const TransitionZoom = React.forwardRef(function Transition(props, ref) {
  return <Zoom direction="up" ref={ref} {...props} />;
});

const styles = theme => ({
  root: {
    padding: theme.spacing(6),
    backgroundColor: 'white',
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  first: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  second: {
    display: 'flex',
    flexDirection: 'column',
  },
  categoryName: {
    fontWeight: 'bold',
    fontSize: 15,
    paddingLeft: 15,
  },
  result: {
    fontSize: 15,
    marginLeft: 5,
    color: 'grey',
  },
  comment: {
    fontSize: 15,
    padding: 15,
    fontStyle: 'italic',
    color: 'black',
  },
  commentTitle: {
    fontWeight: 'bold',
    fontSize: 15,
    marginLeft: 10,
    padding: 15,
  },
  image: {
    backgroundSize: 'cover',
    backgroundPosition: 'top center',
  },
});

class Favourite extends Component {
  state = {
    id: 0,
    name: '',
    type: '',
    description: '',
    picture: '',
    average_cost: 0,
    score: 0,
    open: false,
  };

  openDialog = () => {
    this.setState({ open: true });
  };
  closeDialog = () => {
    this.setState({ open: false });
  };

  componentDidMount() {
    axios({
      method: 'get',
      url: process.env.REACT_APP_API_URL + '/dashboard/get/favorite/dish',
      headers: {
        Authorization: 'Bearer ' + sessionStorage.getItem('token'),
      },
    }).then(res => {
      this.setState({
        name: res.data.dish.name,
        type: res.data.dish.type,
        score: res.data.waste,
        description: res.data.dish.description,
        picture: res.data.dish.picture,
        average_cost: res.data.dish.averageCost,
      });
    });
  }

  colorGradient(note) {
    if (note <= 20) {
      return 'green';
    } else if (note < 50) {
      return 'orange';
    } else {
      return 'red';
    }
  }

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <div className={classes.first}>
          <div className={classes.second}>
            <Typography gutterBottom variant="h5">
              Plat le plus aimé:{' '}
            </Typography>
            <Typography gutterBottom variant="subtitle1" color="textSecondary">
              Le plat le plus apprécié est
              <Typography gutterBottom style={{ fontWeight: 'bold' }} display="inline">
                {' ' + this.state.name + ' '}
              </Typography>
              avec un score de gaspillage de
              <Typography gutterBottom style={{ fontWeight: 'bold' }} display="inline">
                {' ' + this.state.score + '!'}
              </Typography>
            </Typography>
            <Link onClick={() => this.openDialog()}>
              <Typography variant="subtitle1" color="textSecondary">
                Cliquez ici pour consulter les informations concernant ce plat.
              </Typography>
            </Link>
          </div>
          <ThumbUpIcon style={{ fontSize: 80 }} />
        </div>
        <Dialog
          open={this.state.open}
          onClose={() => {
            this.closeDialog();
          }}
          TransitionComponent={TransitionZoom}
        >
          <div
            style={{
              display: 'flex',
              width: 600,
              height: 200,
              marginTop: 50,
              justifyContent: 'flex-start',
            }}
          >
            <div
              className={classes.image}
              style={{
                backgroundImage: `url(${
                  this.state.picture
                    ? this.state.picture
                    : 'https://www.immobilier-lagrandemotte.com/wp-content/themes/realestate-7/images/no-image.png'
                })`,
                width: '200px',
                height: '200px',
              }}
            ></div>
            <div
              style={{
                display: 'flex',
                width: 400,
                flexDirection: 'column',
                justifyContent: 'flex-start',
                backgroundColor: '#F5F5F5',
              }}
            >
              <div
                style={{
                  display: 'flex',
                  height: 30,
                  paddingTop: 20,
                  paddingRight: 5,
                  alignItems: 'center',
                  justifyContent: 'flex-end',
                }}
              ></div>
              <div style={{ display: 'flex', alignItems: 'center' }}>
                <p className={classes.categoryName}>Plat:</p>
                {
                  <p className={classes.result}>
                    {this.state.name} ({this.state.type})
                  </p>
                }
              </div>
              <div style={{ display: 'flex', alignItems: 'center' }}>
                <p className={classes.categoryName}>Budget moyen par assiette:</p>
                {<p className={classes.result}>{this.state.average_cost}</p>}
              </div>
              <div style={{ display: 'flex', alignItems: 'center' }}>
                <p className={classes.categoryName}>Score de gaspillage:</p>
                <p className={classes.result} style={{ color: this.colorGradient(this.score) }}>
                  {this.state.score}
                </p>
              </div>
            </div>
          </div>
          <div
            style={{
              display: 'flex',
              width: 600,
              height: 146,
              backgroundColor: '#DBDFDB',
            }}
          >
            <p className={classes.commentTitle}>Commentaires:</p>
            <p className={classes.comment}>{this.state.description ? this.state.description : 'Aucun commentaire.'}</p>
          </div>
        </Dialog>
      </div>
    );
  }
}

export default withStyles(styles)(Favourite);
