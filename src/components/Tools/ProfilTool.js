import React, { Component } from 'react';
import { withStyles } from '@material-ui/core';
import axios from 'axios';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import lycee from '../../media/lycee.jpg'

const styles = theme => ({
  root: {
    padding: theme.spacing(6),
    backgroundColor: 'white',
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  first: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  second: {
    display: 'flex',
    flexDirection: 'column',
  },
});

class ProfilTool extends Component {
  state = {
    nom: 'Ecole du moulin',
    ville: 'Lyon',
    adresse: 'Place Verdun BP-1004',
    CP: '05000',
    academie: 'Aix-Marseille',
    contact: '+33 4 92 52 26 91',
    wasteNote: '34',
    classement: 8,
    nbTotal: 10,
    costumerNote: '8.1',
  };

  getRank() {
    axios({
      method: 'get',
      url: process.env.REACT_APP_API_URL + '/dashboard/establishment/ranking',
      headers: {
        Authorization: 'Bearer ' + sessionStorage.getItem('token'),
      },
    }).then(res => {
      this.setState({ classement: res.data.position, nbTotal: res.data.total });
    });
  }

  getWaste() {
    axios({
      method: 'get',
      url: process.env.REACT_APP_API_URL + '/dashboard/get/establishment/waste',
      headers: {
        Authorization: 'Bearer ' + sessionStorage.getItem('token'),
      },
      params: {},
    }).then(res => {
      this.setState({ wasteNote: res.data.waste });
    });
  }

  componentDidMount() {
    this.getRank();
    this.getWaste();
  }

  fileUploadAction = () => {
    this.inputReference.current.click();
  };

  fileUploadInputChange = e => {
    var file = e.target.files[0];
    var reader = new FileReader();
    reader.onload = this.test;
    reader.readAsDataURL(file);
  };

  test(e) {
    this.setState({ photo: e.target.result });
  }

  render() {
    const { classes } = this.props;

    return (
      <div style={{...styleFlo.container}}>
        <div style={styleFlo.card}>
          <div style={{...styleFlo.ltitle, paddingTop: 0}}>
          
            <Grid
                container
                direction="row"
                justify="center"
                alignItems="flex-start"
                style={{flexGrow: 1}}
                spacing={20}
            >
            <Grid item xs={4}>
                <Button>
                <img onClick={() => {this.setState({open2: true})}} style={styleFlo.grid.photo} src={lycee}/>
                </Button>
                <div style={styleFlo.grid.tag}>{this.state.nom}</div>
                <div style={styleFlo.grid.writtenby}>{'Classement: ' + this.state.classement}</div>
                <br/>
                <div style={styleFlo.grid.writtenby}>{'Note de gaspillage: '}</div>
                <div style={styleFlo.grid.title}>{this.state.wasteNote}</div>
                </Grid>
                </Grid>
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(ProfilTool);

var styleFlo = {
  hr: {
    color: "grey"
  },
  grid: {
    title: {
      paddingTop: 15,
      fontSize: 23,
      fontWeight: 600,
      textAlign: "center",
      height: 140
    },
    tag: {
      paddingTop: 30,
      color: "green",
      fontSize: 15,
      fontWeight: 600,
      textAlign: "center"
    },
    writtenby: {
      paddingTop: 20,
      fontSize: 15,
      fontWeight: 400,
      textAlign: "center"
    },
    photo: {
      paddingTop: 0,
      width: 200,
      height: 200,
      borderRadius: 20
    },
    like: {
      textAlign: "right",
      fontWeight: 800
    
    }
  },
  already: {
    display: "flex",
    marginTop: "50px",
    justifyContent: "center",
    width: "100%",
    alignItems: "center"
  },
  containerTextField: {
    justifyContent: "center",
    alignItems: "center",
    display: "flex",
    width: "100%",
  },
  textfield: {
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
  },
  containerImage: {
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 20,
    width: "100%"
  },
  title: {
    fontSize: 30,
    fontWeight: 600,
    textAlign: "center"
  },
  ltitle: {
    fontSize: 20,
    fontWeight: 300,
    textAlign: "center"
  },
  logo: {
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    paddingtop: 30,
    width: "50px",
    height: "50px",
    textAlign: "center",
    justifyContent: "center",
    alignItems: "center"
  },
  container: {
    width: "100%",
    height: "100%",
    paddingTop: 100,
    backgroundRepeat  : 'no-repeat',
    backgroundPosition: 'center',
    backgroundSize: 'cover', 
    backgroundColor: 'white',
    display: "flex",
    justifyContent: "center",
  },
  card: {
    width: "100%",
    height: "80%",
    backgroundColor: "white",
    borderRadius: 5,
    justifyContent: "center",
    alignItems: "center",
    //boxShadow: "2px 2px 20px 2px black"

  },
  text: {
    fontSize: 30,
    textAlign: "center",
    fontWeight: 400,
    paddingTop: 50
  },
  button: {
    backgroundColor: "red",
    fontSize: "20px",
    marginRight: 20,
    color: "white"
  },
  secondButton: {
    border: "2px solid black",
    fontSize: "10px",
    marginLeft: 20
  },
  containerButton: {
    justifyContent: "center",
    alignItems: "center",
    display: "flex",
    marginTop: 150
  }
};