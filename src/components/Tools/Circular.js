import React, { Component } from 'react';
import { withStyles, Card, CardContent, Grid } from '@material-ui/core';

import { Doughnut } from 'react-chartjs-2';
import axios from 'axios';
import Typography from '@material-ui/core/Typography';

const test = [25, 25, 25, 25];
const data = {
    labels: ['Entrée', 'Viande et poisson', 'Accompagnement', 'Dessert'],
    datasets: [
      {
        label: '# of Votes',
        data: test,
        backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(255, 159, 64, 0.2)',
        ],
        borderColor: [
          'rgba(255, 99, 132, 1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)',
        ],
        borderWidth: 1,
      },
    ],
  };

  const styles = theme => ({
    root: {
      padding: theme.spacing(4),
    },
    block: {
      backgroundColor: 'white',
      color: 'white',
      borderColor: 'grey',
      padding: '20px',
    },
    title: {
      textAlign: 'center',
      color: 'black',
    },
  });

class Circular extends Component {
    constructor(props) {
        super(props);
        this.state = {
            entree: 10,
            viande: 25,
            acc: 30,
            dessert: 35,
            entreePourc: 0,
            viandePourc: 0,
            accPourc: 0,
            dessertPourc: 0,
            data: data,
            wastedMost: '*********'
        }
    }
    getType = (type) => {
      axios({
        method: 'get',
        url: process.env.REACT_APP_API_URL + '/dashboard/get/waste/by/type',
        headers: {
          Authorization: 'Bearer ' + sessionStorage.getItem('token'),
        },
        params: {
          type: type,
        }
      }).then ((res) => {
        var data = res.data.waste;
        if (data == null)
          data = 0;
        if (type == 'entree')
          this.setState({entree: data});
        else if (type == 'accompagnement')
          this.setState({acc: data});
        else if (type == 'dessert')
          this.setState({dessert: data});
        else if (type == 'viande')
          this.setState({viande: data});
      }, (err) => {
        alert('Erreur de récupération du pourcentage de gaspillage des ' + type + '.');
      })
    }

    mostWasted = (entree, viande, acc, dessert) => {
      if (entree > viande && entree > acc && entree > dessert)
        this.setState({wastedMost: 'entrées'});
      else if (viande > entree && viande > acc && viande > dessert)
        this.setState({wastedMost: 'viandes et poissons'});
      else if (acc > viande && acc > entree && acc > dessert)
        this.setState({wastedMost: 'accompagnements'});
      else if (dessert > viande && dessert > acc && dessert > entree)
        this.setState({wastedMost: 'desserts'});
      else if (dessert == viande && dessert != 0)
      this.setState({wastedMost: 'viande, poissons et desserts'});
      else if (dessert == entree  && dessert != 0)
      this.setState({wastedMost: 'entrées et desserts'});
      else if (dessert == acc  && dessert != 0)
      this.setState({wastedMost: 'accompagnements et desserts'});
      else if (entree == viande  && entree != 0)
      this.setState({wastedMost: 'entrées, viande et poissons'});
      else if (entree == acc  && entree != 0)
      this.setState({wastedMost: 'entrées et accompagnements'});
      else if (viande == acc  && viande != 0)
      this.setState({wastedMost: 'viande, poissons et accomagnements'});
    }

    computePourc = () => {
      var x = this.state.entree;
      var y = this.state.viande;
      var z = this.state.acc;
      var a = this.state.dessert;
      const total = a + x + y + z;

      var entreePourc = Math.round(x/total * 100);
      var viandePourc = Math.round(y/total * 100);
      var accPourc = Math.round(z/total * 100);
      var dessertPourc = Math.round(a/total * 100);
      this.mostWasted(entreePourc, viandePourc, accPourc, dessertPourc);
      var pourcentages = [entreePourc, viandePourc, accPourc, dessertPourc];
      this.updateData(pourcentages);
    }

    updateData = (pourc) => {
      var data = {
        labels: ['Entrée', 'Viande et poisson', 'Accompagnement', 'Dessert'],
        datasets: [
          {
            label: '# of Votes',
            data: pourc,
            backgroundColor: [
              'rgba(255, 99, 132, 0.2)',
              'rgba(54, 162, 235, 0.2)',
              'rgba(255, 206, 86, 0.2)',
              'rgba(75, 192, 192, 0.2)',
              'rgba(153, 102, 255, 0.2)',
              'rgba(255, 159, 64, 0.2)',
            ],
            borderColor: [
              'rgba(255, 99, 132, 1)',
              'rgba(54, 162, 235, 1)',
              'rgba(255, 206, 86, 1)',
              'rgba(75, 192, 192, 1)',
              'rgba(153, 102, 255, 1)',
              'rgba(255, 159, 64, 1)',
            ],
            borderWidth: 1,
          },
        ],
      };
      this.setState({data: data});
    }

    getInfos = () => {
      this.getType('entree');
      this.getType('viande');
      this.getType('accompagnement');
      this.getType('dessert');
    };

    componentDidMount() {
      this.getInfos();
    }

    render() {
        const { classes } = this.props;
        return (
            <Card>
                
                <div style={{marginLeft: 50}}>
                <Typography gutterBottom variant="subtitle1" color="textSecondary">
              Les 
              <Typography gutterBottom style={{ fontWeight: 'bold' }} display="inline">
              {' ' + this.state.wastedMost + ' '}
              </Typography>
              sont le type d'aliments le plus gaspillé.
              <button style={{marginLeft:40}}onClick={this.computePourc}>Calculer</button>
            </Typography>
             </div>
                <CardContent>
                    <Doughnut data={this.state.data} />
                </CardContent>
            </Card>
        )
    }
}

export default withStyles(styles)(Circular);