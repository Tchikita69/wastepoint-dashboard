import React, { Component } from 'react';
import { withStyles } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';

import axios from 'axios';

const styles = theme => ({
  root: {
    padding: theme.spacing(6),
    backgroundColor: 'white',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  image: {
    backgroundSize: 'cover',
    backgroundPosition: 'top center',
  },
});

class Add extends Component {
  constructor(props) {
    super(props);
    this.state = {
      photo: '',
      name: '',
      budget: '',
      uploaded: false,
      type: 'plat',
      comment: '',
      checked: false,
    };
    this._handleNameChange = this._handleNameChange.bind(this);
    this._handleBudgetChange = this._handleBudgetChange.bind(this);
    this._handleCommentChange = this._handleCommentChange.bind(this);
    this._handleTypeChange = this._handleTypeChange.bind(this);
    this.inputReference = React.createRef();
    this.test = this.test.bind(this);
  }

  _handleNameChange(e) {
    this.setState({ name: e.target.value });
  }

  _handleTypeChange(e) {
    this.setState({ type: e.target.value });
  }

  _handleBudgetChange(e) {
    this.setState({ budget: e.target.value });
  }

  _handleCommentChange(e) {
    this.setState({ comment: e.target.value });
  }

  test(e) {
    var cible = e.target.result;
    this.setState({ photo: cible });
  }

  fileUploadAction = () => {
    this.inputReference.current.click();
  };

  fileUploadInputChange = e => {
    var file = e.target.files[0];
    var reader = new FileReader();
    reader.onload = this.test;
    reader.readAsDataURL(file);
  };

  changePDJ = () => {
    if (!this.state.checked) {
      this.setState({ checked: true });
    } else {
      this.setState({ checked: false });
    }
  };

  postDish() {
    const JSONparse = JSON.stringify({
      name: this.state.name,
      type: this.state.type,
      description: this.state.comment,
      picture: this.state.photo,
      average: this.state.budget,
    });
    axios({
      method: 'post',
      url: process.env.REACT_APP_API_URL + '/dashboard/post/dish',
      headers: {
        Authorization: 'Bearer ' + sessionStorage.getItem('token'),
        'Content-Type': 'application/json',
      },
      data: JSONparse,
    }).then(
      res => {
        alert('Plat ajouté avec succès !');
      }, (err) => {
        alert("Une erreur est survenue lors de l'ajout du plat. Veuillez réessayer.");
      }
    );
  }

  render() {
    const { classes } = this.props;

    if (this.state.uploaded) {
      return (
        <div className={classes.root}>
          <p>Plat ajouté avec succès ! Consultez "L'outil de recherche" pour visualiser ou modifier le plat.</p>
          <button
            style={{ marginTop: 10 }}
            onClick={() => {
              this.setState({
                uploaded: false,
                photo: '',
                name: '',
                budget: '',
                comment: '',
              });
            }}
          >
            Ajouter un nouveau plat
          </button>
        </div>
      );
    }
    return (
      <div className={classes.root}>
        <Typography gutterBottom variant="h5">
          Ajouter un plat
        </Typography>
        <div
          style={{
            display: 'flex',
            justifyContent: 'row',
            width: 500,
            height: 200,
            marginTop: 20,
          }}
        >
          <div
            className={classes.image}
            style={{
              backgroundImage: `url(${
                this.state.photo
                  ? this.state.photo
                  : 'https://www.immobilier-lagrandemotte.com/wp-content/themes/realestate-7/images/no-image.png'
              })`,
              width: '200px',
              height: '200px',
            }}
          >
            <div>
              <input
                type="file"
                hidden
                accept="image/png, image/jpeg"
                ref={this.inputReference}
                onChange={this.fileUploadInputChange}
              />
              <button onClick={this.fileUploadAction}>Ajouter une image</button>
            </div>
          </div>
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'space-around',
              marginLeft: 20,
              width: 300,
            }}
          >
            <TextField
              id="standard-basic"
              label="Nom du plat"
              value={this.state.name}
              onChange={this._handleNameChange}
            />
            <div style={{ marginTop: 10 }}>
              <FormControl required className={classes.formControl} style={{ marginRight: 60 }}>
                <Select
                  labelId="demo-simple-select-required-label"
                  id="demo-simple-select-required"
                  value={this.state.type}
                  onChange={this._handleTypeChange}
                  className={classes.selectEmpty}
                >
                  <MenuItem value="">
                    <em>Aucun</em>
                  </MenuItem>
                  <MenuItem value={'entree'}>Entrée</MenuItem>
                  <MenuItem value={'viande'}>Viande ou poisson</MenuItem>
                  <MenuItem value={'accompagnement'}>Accompagnement</MenuItem>
                  <MenuItem value={'dessert'}>Dessert</MenuItem>
                </Select>
                <FormHelperText>Type</FormHelperText>
              </FormControl>
            </div>
            <TextField
              id="standard-basic"
              label="Budget moyen pour une unité"
              value={this.state.budget}
              onChange={this._handleBudgetChange}
            />
            <TextField
              label="Ajouter un commentaire"
              id="standard-multiline-flexible"
              multiline
              rowsMax={4}
              value={this.state.comment}
              onChange={this._handleCommentChange}
            />
          </div>
        </div>
        <button
          style={{ marginTop: 10 }}
          onClick={() => {
            this.setState({ uploaded: true });
            this.postDish();
          }}
        >
          Envoyer
        </button>
      </div>
    );
  }
}

export default withStyles(styles)(Add);
