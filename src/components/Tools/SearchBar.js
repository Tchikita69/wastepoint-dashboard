import React, { Component } from 'react';
import { withStyles } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import EditIcon from '@material-ui/icons/Edit';
import SaveIcon from '@material-ui/icons/Save';

import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';

import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

import axios from 'axios';

const styles = theme => ({
  root: {
    padding: theme.spacing(2),
    backgroundColor: 'white',
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  first: {
    display: 'flex',
    justifyContent: 'center',
  },
  second: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  categoryName: {
    fontWeight: 'bold',
    fontSize: 13,
    paddingLeft: 15,
  },
  result: {
    fontSize: 13,
    marginLeft: 5,
    color: 'grey',
  },
  comment: {
    fontSize: 15,
    padding: 15,
    fontStyle: 'italic',
    color: 'black',
  },
  commentTitle: {
    fontWeight: 'bold',
    fontSize: 15,
    marginLeft: 10,
    padding: 15,
  },
  image: {
    backgroundSize: 'cover',
    backgroundPosition: 'top center',
  },
});

const disheList = [
  {
    title: 'Spaghettis bolognaises',
    note: 42,
    budget: '0.20 EUR',
    img_uri:
      'https://fac.img.pmdstatic.net/fit/http.3A.2F.2Fprd2-bone-image.2Es3-website-eu-west-1.2Eamazonaws.2Ecom.2Ffac.2F2018.2F07.2F30.2F9eb8894c-aff3-4da5-9bc5-976e2df27889.2Ejpeg/850x478/quality/90/crop-from/center/spaghettis-bolognaise-faciles.jpeg',
    comment: 'Parfait en hiver',
  },
  {
    title: 'Fondant chocolat',
    note: 3,
    budget: '1 EUR',
    img_uri: 'https://img.cuisineaz.com/660x660/2013-12-20/i88719-fondant-au-chocolat-simple.jpeg',
    comment: 'Allergènes: noisettes',
  },
];

function getTodayTimestamp() {
  const dateNow = new Date();
  const year = dateNow.getFullYear();
  const month = dateNow.getMonth() + 1;
  const day = dateNow.getDate();
  const dateToday = year + '-' + month + '-' + day + 'T11:00:00Z';

  const dateTime = new Date(dateToday).getTime();
  const timestamp = Math.floor(dateTime / 1000);
  return timestamp;
}

class SearchBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      notfound: false,
      display: false,
      title: '',
      price: '',
      type: '',
      comment: '',
      photo: '',
      id: 0,
      edit: false,
      dishesList: disheList,
      showedList: disheList,
      openQuit: false,
      filtered: false,
      checked: true,
      pdjIds: []
    };
    this._handleTitleChange = this._handleTitleChange.bind(this);
    this._handlePriceChange = this._handlePriceChange.bind(this);
    this._handleCommentChange = this._handleCommentChange.bind(this);
    this._handleTypeChange = this._handleTypeChange.bind(this);
    this.inputReference = React.createRef();
    this.test = this.test.bind(this);
  }

  getDishes = () => {
    axios({
      method: 'get',
      url: process.env.REACT_APP_API_URL + '/dashboard/get/dishes',
      headers: {
        Authorization: 'Bearer ' + sessionStorage.getItem('token'),
      },
    }).then(
      res => {
        var i = 0;
        var dishes = res.data.dishes;
        var list = [];
        for (i = 0; i < dishes.length; i++) {
          var dish = {
            id: 0,
            type: '',
            title: '',
            note: 42,
            budget: '',
            img_uri: '',
            comment: '',
          };
          dish.id = dishes[i].dish.id;
          dish.type = dishes[i].dish.type;
          dish.title = dishes[i].dish.name;
          dish.note = Math.round(dishes[i].waste);
          dish.img_uri = dishes[i].dish.picture;
          dish.budget = dishes[i].dish.averageCost;
          dish.comment = dishes[i].dish.description;
          list.push(dish);
        }
        this.setState({ dishesList: list });
      },
      err => {
        alert('Erreur du chargement des plats');
      }
    );
  }

  getPDJ = () => {
    axios({
      method: 'get',
      url: process.env.REACT_APP_API_URL + '/dashboard/get/menu',
      headers: {
        Authorization: 'Bearer ' + sessionStorage.getItem('token'),
      },
      params: {
        date: getTodayTimestamp()
      }
    }).then ((res) => {
        var ids = res.data.menu.dishes;
        if (ids == undefined)
          ids = [0];
        this.setState({pdjIds: ids});
        
    }, (err) => {
      alert(err);
    });
  }

  componentDidMount() {
    this.getDishes();
    this.getPDJ();
  }

  test(e) {
    this.setState({ photo: e.target.result });
  }

  _handleTitleChange(e) {
    this.setState({ title: e.target.value });
  }

  _handlePriceChange(e) {
    this.setState({ price: e.target.value });
  }

  _handleCommentChange(e) {
    this.setState({ comment: e.target.value });
  }

  _handleTypeChange(e) {
    this.setState({ type: e.target.value });
  }

  fileUploadAction = () => {
    this.inputReference.current.click();
  };

  fileUploadInputChange = e => {
    var file = e.target.files[0];
    var reader = new FileReader();
    reader.onload = this.test;
    reader.readAsDataURL(file);
  };

  getInfos(value) {
    console.log(value);
    this.setState({ display: false });
    var cmp = 1;
    this.state.dishesList.forEach(obj => {
      if (value === obj.title) {
        // Create a copy of object in selected
        var selected = { ...obj };
        this.setState({
          selected,
          display: true,
          title: obj.title,
          type: obj.type,
          photo: obj.img_uri,
          price: obj.budget,
          comment: obj.comment,
          id: obj.id,
          checked: this.isThisDishPDJ(obj.id)
        });
      }
      cmp++;
    });
  }

  isThisDishPDJ(id) {
    var j = 0;
    var listPDJ = this.state.pdjIds;

    for (j = 0; j < listPDJ.length; j++)
      if (id == listPDJ[j])
        return (1);
    return (0);
  }

  filterPDJs() {
    this.setState({ filtered: true });
    var i = 0;
    var dishes = this.state.dishesList;
    var list = [];
    for (i = 0; i < dishes.length; i++) {
        if (this.isThisDishPDJ(dishes[i].id) == 1) {
          var dish = {
            id: 0,
            type: '',
            title: '',
            note: 42,
            budget: '',
            img_uri: '',
            comment: '',
          };
          dish.id = dishes[i].id;
          dish.type = dishes[i].type;
          dish.title = dishes[i].title;
          dish.note = Math.round(dishes[i].note);
          dish.img_uri = dishes[i].img_uri;
          dish.budget = dishes[i].budget;
          dish.comment = dishes[i].comment;
          list.push(dish);
        }
    }
    this.setState({ showedList: list });
  }

  colorGradient(note) {
    if (note <= 20) {
      return 'green';
    } else if (note < 50) {
      return 'orange';
    } else {
      return 'red';
    }
  }

  saveModification = () => {
    const JSONparse = {
      id: this.state.id,
      name: this.state.title,
      type: this.state.type,
      description: this.state.comment,
      picture: this.state.photo,
      average_cost: this.state.price,
    };
    axios({
      method: 'put',
      url: process.env.REACT_APP_API_URL + '/dashboard/update/dish',
      headers: {
        Authorization: 'Bearer ' + sessionStorage.getItem('token'),
        'Content-Type': 'application/json',
      },
      data: JSONparse,
    }).then(
      res => {
        alert('Plat modifié avec succès !');
      },
      err => {
        alert('Echec de la modification.');
      }
    );
  };

  editOrSave = edit => {
    if (!edit == false) {
      this.saveModification();
    }
    this.setState({ edit: !this.state.edit });
  };

  handleQuitOpen = () => {
    this.setState({ openQuit: true });
  };

  handleQuitClose = () => {
    this.setState({ openQuit: false });
  };

  handleCheck = () => {
    if (!this.state.filtered) {
      this.setState({ filtered: true });
      this.filterPDJs();
    } else {
      this.setState({ filtered: false });
      this.setState({ showedList: this.state.dishesList });
    }
  };

  postNewPdjsList() {
    var test = this.state.pdjIds;
    const JSONparse = JSON.stringify({"ids":test});
    axios({
      method: 'post',
      url: process.env.REACT_APP_API_URL + '/dashboard/post/menu',
      headers: {
        'Authorization': 'Bearer ' + sessionStorage.getItem('token'),
        'Content-Type': 'application/json',
        'Connection': 'keep-alive',
        'Accept':'*/*',
        'Accept-Encoding':'gzip, deflate, br'
      },
      data: JSONparse

    }).then((res) => {
      alert('ok')
    }, (err) => {
      alert("Erreur de changement du menu du jour");
    })
  }

  updateNewPdjsList() {
    var test = this.state.pdjIds;
    var date = getTodayTimestamp();
    const JSONparse = JSON.stringify({"ids":test,"date":date});
    axios({
      method: 'put',
      url: process.env.REACT_APP_API_URL + '/dashboard/update/menu',
      headers: {
        'Authorization': 'Bearer ' + sessionStorage.getItem('token'),
        'Content-Type': 'application/json',
        'Connection': 'keep-alive',
        'Accept':'*/*',
        'Accept-Encoding':'gzip, deflate, br'
      },
      data: JSONparse

    }).then((res) => {
      alert('Menu modifié avec succès.')
    }, (err) => {
      alert("Impossible de modifier le menu.")
    })
  }

  
  addToPDJ() {
    var id = this.state.id;
    var list = this.state.pdjIds;

    list.push(id);
    this.setState({pdjIds: list});
  }

  removeFromPDJ() {
    var id = this.state.id;
    var list = this.state.pdjIds;
    const index = list.indexOf(id);

    list.splice(index, 1);
    this.setState({pdjIds: list});
  }

  changePDJ = () => {
    if (!this.state.checked) {
      this.setState({ checked: true });
      this.addToPDJ();
    } else {
      this.setState({ checked: false });
      this.removeFromPDJ();
    }
    this.updateNewPdjsList();
  };

  delete = () => {
    this.handleQuitClose();
    axios({
      method: 'delete',
      url: process.env.REACT_APP_API_URL + '/dashboard/remove/dish',
      headers: {
        Authorization: 'Bearer ' + sessionStorage.getItem('token'),
      },
      params: {
        id: this.state.id,
      },
    }).then(
      res => {
        alert('Plat supprimé !');
      },
      err => {
        alert('Erreur de la suppression du plat.');
      }
    );
  };

  dispInfos() {
    const { classes } = this.props;

    console.log(this.state.selected);
    return (
      <div>
        <div
          style={{
            display: 'flex',
            width: 600,
            height: 200,
            marginTop: 50,
            justifyContent: 'flex-start',
          }}
        >
          <div
            className={classes.image}
            style={{
              backgroundImage: `url(${
                this.state.photo
                  ? this.state.photo
                  : 'https://www.immobilier-lagrandemotte.com/wp-content/themes/realestate-7/images/no-image.png'
              })`,
              width: '200px',
              height: '200px',
            }}
          >
            {this.state.edit ? (
              <div>
                <input
                  type="file"
                  hidden
                  accept="image/png, image/jpeg"
                  ref={this.inputReference}
                  onChange={this.fileUploadInputChange}
                />
                <button onClick={this.fileUploadAction}>Modifier l'image</button>
              </div>
            ) : null}
          </div>
          <div
            style={{
              display: 'flex',
              width: 400,
              flexDirection: 'column',
              justifyContent: 'flex-start',
              backgroundColor: '#F5F5F5',
            }}
          >
            <div
              style={{
                display: 'flex',
                height: 30,
                paddingTop: 20,
                paddingRight: 5,
                alignItems: 'center',
                justifyContent: 'flex-end',
              }}
            >
              <IconButton
                onClick={() => {
                  this.editOrSave(this.state.edit);
                }}
              >
                {!this.state.edit ? <EditIcon /> : <SaveIcon />}
              </IconButton>
            </div>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <p className={classes.categoryName}>Plat:</p>
              {!this.state.edit ? (
                <p className={classes.result}>{this.state.title}</p>
              ) : (
                <TextField
                  style={{ paddingLeft: 15 }}
                  id="standard-basic"
                  value={this.state.title}
                  onChange={this._handleTitleChange}
                />
              )}
            </div>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <p className={classes.categoryName}>Budget moyen:</p>
              {!this.state.edit ? (
                <p className={classes.result}>{this.state.price}</p>
              ) : (
                <TextField
                  style={{ paddingLeft: 15 }}
                  id="standard-basic"
                  value={this.state.price}
                  onChange={this._handlePriceChange}
                />
              )}
            </div>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <p className={classes.categoryName}>Type:</p>
              {!this.state.edit ? (
                <p className={classes.result}>{this.state.type}</p>
              ) : (
                <FormControl required className={classes.formControl} style={{ paddingLeft: 15 }}>
                  <Select
                    labelId="demo-simple-select-required-label"
                    id="demo-simple-select-required"
                    value={this.state.type}
                    onChange={this._handleTypeChange}
                    className={classes.selectEmpty}
                  >
                    <MenuItem value="">
                      <em>Aucun</em>
                    </MenuItem>
                    <MenuItem value={'entree'}>Entrée</MenuItem>
                    <MenuItem value={'viande'}>Viande ou poisson</MenuItem>
                    <MenuItem value={'accompagnement'}>Accompagnement</MenuItem>
                    <MenuItem value={'dessert'}>Dessert</MenuItem>
                  </Select>
                </FormControl>
              )}
            </div>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <p className={classes.categoryName}>Score de gaspillage:</p>
              <p
                className={classes.result}
                style={{
                  color: this.colorGradient(this.state.selected.note),
                  marginRight: 20,
                }}
              >
                {this.state.selected.note}
              </p>

              <FormControlLabel
                disabled={!this.state.edit}
                control={<Checkbox checked={this.state.checked} onChange={this.changePDJ} name="checkedB" />}
                label="Plat du jour"
              />
            </div>
          </div>
        </div>
        <div
          style={{
            display: 'flex',
            width: 600,
            height: 146,
            backgroundColor: '#DBDFDB',
          }}
        >
          <p className={classes.commentTitle}>Commentaires:</p>
          <p className={classes.comment}>
            {!this.state.edit ? (
              this.state.comment ? (
                this.state.comment
              ) : (
                'Aucun commentaire.'
              )
            ) : (
              <TextField
                style={{ paddingLeft: 15, width: 350 }}
                id="standard-multiline-flexible"
                multiline
                rowsMax={4}
                value={this.state.comment}
                onChange={this._handleCommentChange}
              />
            )}
          </p>
        </div>
        {this.state.edit ? (
          <div style={{ marginTop: 20, marginLeft: 200 }}>
            <button onClick={this.handleQuitOpen}>Supprimer ce plat</button>
          </div>
        ) : null}
        <Dialog open={this.state.openQuit} onClose={this.handleQuitClose} maxWidth={'sm'} fullWidth={true}>
          <DialogTitle>Suppression d'un plat</DialogTitle>
          <DialogContent>
            <DialogContentText>Voulez-vous supprimer {this.state.title} ?</DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button autoFocus onClick={this.handleQuitClose} color="primary">
              Annuler
            </Button>
            <Button onClick={this.delete} color="primary">
              OK
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.root}>
        <div className={classes.first}>
          <div className={classes.second}>
            <Typography gutterBottom variant="h5">
              Outil de recherche
            </Typography>
            <div style={{ display: 'flex', flexDirection: 'row' }}>
              <Autocomplete
                id="search"
                options={this.state.filtered ? this.state.showedList : this.state.dishesList}
                getOptionLabel={option => option.title}
                style={{ width: 500, marginTop: 20 }}
                renderInput={params => <TextField {...params} label="Rechercher un plat" variant="outlined" />}
              />
              <IconButton
                style={{ marginTop: 17 }}
                onClick={() => {
                  this.getInfos(document.getElementById('search').value);
                }}
              >
                <SearchIcon fontSize="large" />
              </IconButton>
            </div>
            <div>
              <FormControlLabel
                control={<Checkbox checked={this.state.filtered} onChange={this.handleCheck} name="checkedA" />}
                label="Uniquement les plats du jour"
              />
            </div>
            {this.state.display ? this.dispInfos() : null}
          </div>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(SearchBar);
