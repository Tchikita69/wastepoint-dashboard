import React, { Component } from 'react';
import { withStyles, Card, CardContent, Grid } from '@material-ui/core';

import { Bar, Line } from 'react-chartjs-2';

import Helmet from 'react-helmet';
import moment from 'moment';
import DayPicker from 'react-day-picker';
import 'react-day-picker/lib/style.css';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import 'react-day-picker/lib/style.css';
import axios from 'axios';
import { ok } from 'assert';

function getTimestamp(date) {
  //const dateNow = new Date();
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();
  const dateChosen = year + '-' + month + '-' + day + 'T11:00:00Z';

  const dateTime = new Date(dateChosen).getTime();
  const timestamp = Math.floor(dateTime / 1000);
  return timestamp;
}

Date.prototype.getWeek = function() {
  var onejan = new Date(this.getFullYear(),0,1);
  var today = new Date(this.getFullYear(),this.getMonth(),this.getDate());
  var dayOfYear = ((today - onejan + 86400000)/86400000);
  return Math.ceil(dayOfYear/7)
};

function getDayandMonth(date) {
  var month = date.getMonth() + 1;
  var day = date.getDate();
  if (day < 10)
    day = '0'+day;
  if (month < 10)
    month = '0' + month;
  const dayAndMonth = day + '/' + month;
  return (dayAndMonth);
}

function dateToTimestamp(days) {
  var timestamps = [];
  for (var i = 1; i < 7; i++) {
    var timestamp = getTimestamp(days[i]);
    timestamps.push(timestamp);
  }
  //alert (timestamps)
  return (timestamps);
}

const somedata = [
    {
      week: 39,
      popol: [50, 80, 12, 40, 15],
      grammes: [7600, 10600, 3200, 5400, 2000],
    },
    {
      week: 40,
      popol: [12, 27, 51, 17, 43],
      grammes: [3400, 5230, 8978, 2126, 2123],
    },
    {
      week: 41,
      popol: [15, 50],
      grammes: [1200, 5600],
    },
  ];

const styles = theme => ({
    root: {
      padding: theme.spacing(4),
    },
    block: {
      backgroundColor: 'white',
      color: 'white',
      borderColor: 'grey',
      padding: '20px',
    },
    title: {
      textAlign: 'center',
      color: 'black',
    },
  });

const MONTHS = [
    'Janvier',
    'Février',
    'Mars',
    'Avril',
    'Mai',
    'Juin',
    'Juillet',
    'Août',
    'Septembre',
    'Octobre',
    'Novembre',
    'Décembre',
  ];
  const WEEKDAYS_LONG = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];
  
  const WEEKDAYS_SHORT = ['Di', 'Lu', 'Ma', 'Me', 'Je', 'Ve', 'Sa'];

  class WeekChart2 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            options: {
                scales: {
                  yAxes: [
                    {
                      ticks: {
                        beginAtZero: true,
                      },
                    },
                  ],
                },
                pan: {
                  enabled: true,
                  mode: 'xy',
                },
                zoom: {
                  enabled: true,
                  mode: 'xy',
                },
              },
              data: {
                labels: ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi'],
                datasets: [
                  {
                    label: 'Taux de gaspillage en %',
                    data: somedata[2].popol,
                    backgroundColor: [
                      'rgb(255,245,221)',
                      'rgb(219,242,242)',
                      'rgb(215,236,251)',
                      'rgb(235,224,255)',
                      'rgb(255,236,217)',
                    ],
                  },
                ],
              },
              dataKG: {
                labels: ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi'],
                datasets: [
                  {
                    label: 'Taux de gaspillage en kg',
                    data: [0,0,0,0,0],
                    backgroundColor: [
                      'rgb(255,245,221)',
                      'rgb(219,242,242)',
                      'rgb(215,236,251)',
                      'rgb(235,224,255)',
                      'rgb(255,236,217)',
                    ],
                  },
                ],
              },
              current: somedata[2],
        
              percent: true,
              charged: false,
              open: false,
        
              hoverRange: undefined,
              selectedDays: [],
              week: somedata[2].week,
              from: '05/10',
              to: '09/10',
        
              something: [{ week: somedata[2].week, percents: [15, 50], grammes: [1200, 5600] }],
              chart: true,
              monthMode: false,
            };
        }

    getInfos = (timestamps) => {
      var period = timestamps[0]+','+timestamps[5];
      var wastes = [];
      axios({
        method: 'get',
        url: process.env.REACT_APP_API_URL + '/dashboard/get/waste/over/time',
        headers: {
          Authorization: 'Bearer ' + sessionStorage.getItem('token'),
        },
        params: {
          period: period
        }
      }).then ((res) => {
        for (var i = 0; i < 5; i++) {
          var wasteDay = res.data[i].waste;
          (wasteDay == null) ? wastes.push(0):wastes.push(Math.round(wasteDay));
        }
        var data = {
          labels: ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi'],
          datasets: [
            {
              label: 'Taux de gaspillage en %',
              data: wastes,
              backgroundColor: [
                'rgb(255,245,221)',
                'rgb(219,242,242)',
                'rgb(215,236,251)',
                'rgb(235,224,255)',
                'rgb(255,236,217)',
              ],
            },
          ],
        }
        this.setState({data: data});
      }, (err) => {
        alert("Impossible de récupérer le gaspillage quotidien.");
      });
    }

    updateDates = (days) => {
      var from = getDayandMonth(days[0]);
      var to = getDayandMonth(days[6]);
      this.setState({from: from, to: to, week: days[0].getWeek()});
    }

    handleDayChange = date => {
      var days = this.getWeekDays(this.getWeekRange(date).from);
        this.setState({
          selectedDays: days,
        });
        this.updateDates(days);
        var timestamps = dateToTimestamp(days);
        this.getInfos(timestamps);
      };
    
      handleDayEnter = date => {
        this.setState({
          hoverRange: this.getWeekRange(date),
        });
      };
    
      handleDayLeave = () => {
        this.setState({
          hoverRange: undefined,
        });
    };

    handleWeekClick = (weekNumber, days, e) => {
        var fromDate = new Date(days[0]);
        var toDate = new Date(days[4]);

        this.setState({date0: days[0], date4: days[4]})
    
        var fromTmp =
          ('0' + fromDate.getDate()).slice(-2).toString() + '/' + ('0' + (fromDate.getMonth() + 1)).slice(-2).toString();
        var toTmp =
          ('0' + toDate.getDate()).slice(-2).toString() + '/' + ('0' + (toDate.getMonth() + 1)).slice(-2).toString();
    
        this.setState({
          from: fromTmp,
          to: toTmp,
          selectedDays: days,
          week: weekNumber,
          monthMode: false,
        });
        this.changeData(weekNumber, this.state.percent, true);
      };

      validate = () => {
        this.setState({open: false});
      }

      dialogRender() {
        const { hoverRange, selectedDays } = this.state;
    
        const daysAreSelected = selectedDays.length > 0;
    
        const modifiers = {
          hoverRange,
          selectedRange: daysAreSelected && {
            from: selectedDays[0],
            to: selectedDays[6],
          },
          hoverRangeStart: hoverRange && hoverRange.from,
          hoverRangeEnd: hoverRange && hoverRange.to,
          selectedRangeStart: daysAreSelected && selectedDays[0],
          selectedRangeEnd: daysAreSelected && selectedDays[6],
        };
    
        return (
          <Dialog open={this.state.open} onClose={() => this.setState({ open: false })}>
            <DialogTitle>Choisir une semaine</DialogTitle>
            <DialogContent style={{ height: 400, width: 400 }}>
              <div className="SelectedWeekExample">
                <DayPicker
                  selectedDays={selectedDays}
                  showWeekNumbers
                  showOutsideDays
                  modifiers={modifiers}
                  months={MONTHS}
                  weekdaysLong={WEEKDAYS_LONG}
                  weekdaysShort={WEEKDAYS_SHORT}
                  onDayClick={this.handleDayChange}
                  onDayMouseEnter={this.handleDayEnter}
                  onDayMouseLeave={this.handleDayLeave}
                  onWeekClick={this.handleWeekClick}
                />
                {selectedDays.length === 7 && (
                  <div>
                    {getDayandMonth(this.state.selectedDays[0])} – {getDayandMonth(this.state.selectedDays[6])}
                  
                  <button style={{marginLeft: 50}} onClick={this.validate}>OK</button>
                  </div>
                )}
    
                <Helmet>
                  <style>{`
                .SelectedWeekExample .DayPicker-Month {
                  border-collapse: separate;
                }
                .SelectedWeekExample .DayPicker-WeekNumber {
                  outline: none;
                }
                .SelectedWeekExample .DayPicker-Day {
                  outline: none;
                  border: 1px solid transparent;
                }
                .SelectedWeekExample .DayPicker-Day--hoverRange {
                  background-color: #EFEFEF !important;
                }
    
                .SelectedWeekExample .DayPicker-Day--selectedRange {
                  background-color: #fff7ba !important;
                  border-top-color: #FFEB3B;
                  border-bottom-color: #FF    const updated = {
                    ...this.state.data,
                    datasets: [
                      {
                        label: "Taux de gaspillage en g",
                        data: currentData,
                        backgroundColor: ["rgb(255,245,221)", "rgb(219,242,242)", 
                                          "rgb(215,236,251)", "rgb(235,224,255)", "rgb(255,236,217)"],
                      }
                    ]
                  }EB3B;
                  border-left-color: #fff7ba;
                  border-right-color: #fff7ba;
                }
    
                .SelectedWeekExample .DayPicker-Day--selectedRangeStart {
                  background-color: #FFEB3B !important;
                  border-left: 1px solid #FFEB3B;
                }
    
                .SelectedWeekExample .DayPicker-Day--selectedRangeEnd {
                  background-color: #FFEB3B !important;
                  border-right: 1px solid #FFEB3B;
                }
    
                .SelectedWeekExample .DayPicker-Day--selectedRange:not(.DayPicker-Day--outside).DayPicker-Day--selected,
                .SelectedWeekExample .DayPicker-Day--hoverRange:not(.DayPicker-Day--outside).DayPicker-Day--selected {
                  border-radius: 0 !important;
                  color: black !important;
                }
                .SelectedWeekExample .DayPicker-Day--hoverRange:hover {
                  border-radius: 0 !important;
                }
              `}</style>
                </Helmet>
              </div>
            </DialogContent>
          </Dialog>
        );
      }

      getWeekDays(weekStart) {
        const days = [weekStart];
        for (let i = 1; i < 7; i += 1) {
          days.push(moment(weekStart).add(i, 'days').toDate());
        }
        return days;
      }
    
      getWeekRange(date) {
        return {
          from: moment(date).startOf('week').toDate(),
          to: moment(date).endOf('week').toDate(),
        };
      }
    
      changeData(week, percent, newWeek = false) {
        const test = [
          {
            week: 39,
            percents: [50, 80, 12, 40, 15],
            grammes: [7600, 10600, 3200, 5400, 2000],
            month: 'September',
          },
          {
            week: 40,
            percents: [12, 27, 51, 17, 43],
            grammes: [3400, 5230, 8978, 2126, 2123],
            month: 'September',
          },
          {
            week: 41,
            percents: [15, 50],
            grammes: [1200, 5600],
            month: 'October',
          },
        ];
        var tmp = [];
        const test2 = test.find(x => x.week === week);
        tmp.push(test2);
    
        console.log(test2);
        const updated = {
          ...this.state.dataKG,
          datasets: [
            {
              label: 'Taux de gaspillage en %',
              data: test2.percents,
              backgroundColor: [
                'rgb(255,245,221)',
                'rgb(219,242,242)',
                'rgb(215,236,251)',
                'rgb(235,224,255)',
                'rgb(255,236,217)',
              ],
            },
          ],
        };
    
        this.setState({ data: updated, something: tmp, percent: true });
      }
    
      monthData(month) {
        var monthData;
        if (month === 'october') {
          monthData = [
            {
              week: 41,
              percents: [15, 50],
              grammes: [1200, 5600],
              month: 'october',
            },
          ];
        } else {
          monthData = [
            {
              week: 39,
              percents: [50, 80, 12, 40, 15],
              grammes: [7600, 10600, 3200, 5400, 2000],
              month: 'september',
            },
            {
              week: 40,
              percents: [12, 27, 51, 17, 43],
              grammes: [3400, 5230, 8978, 2126, 2123],
              month: 'september',
            },
          ];
        }
    
        var newDataset = [];
    
        monthData.forEach(element => {
          newDataset.push({
            label: 'Semaine ' + element.week,
            data: element.percents,
            backgroundColor: [
              'rgb(255,245,221)',
              'rgb(219,242,242)',
              'rgb(215,236,251)',
              'rgb(235,224,255)',
              'rgb(255,236,217)',
            ],
          });
        });
    
        const updated = {
          ...this.state.dataKG,
          labels: ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi'],
          datasets: newDataset,
        };
        this.setState({
          data: updated,
          something: monthData,
          percent: true,
          monthMode: true,
        });
      }
    
      switchUnit(percent) {
        var newDataset = [];
    
        this.state.something.forEach(element => {
          newDataset.push({
            label:
              this.state.something.length === 1
                ? !percent
                  ? 'Taux de gaspillage en %'
                  : 'Taux de gaspillage en g'
                : element.week,
            data: !percent ? element.percents : element.grammes,
            backgroundColor: [
              'rgb(255,245,221)',
              'rgb(219,242,242)',
              'rgb(215,236,251)',
              'rgb(235,224,255)',
              'rgb(255,236,217)',
            ],
          });
        });
    
        const updated = {
          ...this.state.dataKG,
          labels: ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi'],
          datasets: newDataset,
        };
        this.setState({ data: updated, percent: !percent });
      }
    
      mergeMonthData() {
        var newLabels = [];
        var newDataSet = [];
    
        this.state.something.forEach(element => {
          element.percents.forEach(percent => {
            newLabels.push(element.week);
            newDataSet.push(percent);
          });
        });
    
        console.log(newDataSet);
        const updated = {
          ...this.state.dataKG,
          labels: newLabels,
          datasets: [
            {
              label: 'Évolution',
              data: newDataSet,
            },
          ],
        };
        this.setState({ data: updated });
      }

      render() {
        const { classes } = this.props;
    
        return (
          <Card className={classes.block}>
            <div style={{ display: 'flex', justifyContent: 'space-around' }}>
              <div id="tabtitle" className={classes.title}>
                Evolution du taux de gaspillage - Semaine {this.state.week} du ({this.state.from} au {this.state.to})
              </div>
              <button
                onClick={() => {
                  this.setState({ open: true });
                }}
              >
                Sélectionner une semaine
              </button>
              {this.dialogRender()}
            </div>
            <CardContent>
              <Grid>
                <div className="canvas-container">
                    <Bar options={this.state.options} data={this.state.data} />
                </div>
              </Grid>
            </CardContent>
          </Card>
        );
      }
    }

  export default withStyles(styles)(WeekChart2);
