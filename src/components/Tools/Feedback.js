import React, { Component } from 'react';
import MUIDataTable from 'mui-datatables';
import axios from 'axios';
import { wait } from '@testing-library/dom';

const columns = ['Plat', 'Date', 'Commentaire'];

const data = [
  ['Poulet', '01-02-2020', "C'était exquis, et la sauce était incroyable, miam !"],
  ['Spaghettis bolognaises', '02-02-2020', 'Vraiment honteux de payer un service et de manger ça.'],
  ['Spaghettis bolognaises', '02-02-2020', 'Berk, la consistence des pâtes est vraiment à revoir...'],
  ['Haricots verts', '01-02-2020', 'Simple mais efficace.'],
  ['Frites', '03-02-2020', 'Trop trop bons les frites ! On en veut encore'],
  ['Frites', '03-02-2020', "Toujours sympa les frites, attention cependant, c'est la deuxième fois cette semaine."],
  ['Soupe de prolétaire', '04-02-2020', "Miam que c'était bon."],
  ['Fondant chocolat', '04-02-2020', "J'adoooooooore"],
  ['Fondant chocolat', '04-02-2020', "j'aime pas"],
  ['Fondant chocolat', '04-02-2020', 'Faites tout le temps ça svp'],
  ['Boeuf bourguignon', '05-02-2020', "Je suis fier d'être français quand je mange ça !"],
  ['Boeuf bourguignon', '05-02-2020', 'Erk.'],
];

const options = {
  filterType: 'checkbox',
  rowsPerPage: 5,
  rowsPerPageOptions: [5],
};

function timeConverter(UNIX_timestamp) {
  var a = new Date(UNIX_timestamp * 1000);
  var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var time = date + '-' + month + '-' + year; /* + ' ' + hour + ':' + min + ':' + sec */
  return (time);
}

export default class Feedback extends Component {
  state = {
    names: [
      {
        id: 1,
        name: 'oups',
      },
    ],
    data: [['1', '01-02-2020', "Beaucoup trop d'attente."]],
  };
  getInfos = () => {
    var data = [];
    axios({
      method: 'get',
      url: process.env.REACT_APP_API_URL + '/dashboard/feedbacks/dish',
      headers: {
        Authorization: 'Bearer ' + sessionStorage.getItem('token'),
      },
      params: {
        id: 1,
      },
    }).then(
      res => {
        var i = 0;
        var result = [];
        data = res.data.feedbacks;
        for (i = 0; i < data.length; i++) {
          var advice = [];
          advice.push(data[i].title);
          advice.push(timeConverter(data[i].date));
          advice.push(data[i].body);
          result.push(advice);
        }
        const rev = result.reverse();
        this.setState({ data: rev });
      },
      err => {
        alert('Erreur de chargement des avis sur les plats.');
      }
    );
  };
  componentDidMount() {
    this.getInfos();
  }
  render() {
    return <MUIDataTable title={''} data={this.state.data} columns={columns} options={options} />;
  }
}
