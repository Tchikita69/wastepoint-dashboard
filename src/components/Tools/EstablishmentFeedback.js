import React, { Component } from 'react';
import MUIDataTable from 'mui-datatables';
import axios from 'axios';

const columns = ['Note', 'Date', 'Commentaire'];

const options = {
  filterType: 'checkbox',
  rowsPerPage: 5,
  rowsPerPageOptions: [5],
};

function timeConverter(UNIX_timestamp) {
  var a = new Date(UNIX_timestamp * 1000);
  var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var time = date + '-' + month + '-' + year;
  return (time);
}

export default class EstablismentFeedback extends Component {
  state = {
    data: [['5/10', '01-02-2020', "Beaucoup trop d'attente."]],
  };
  componentDidMount() {
    var data = [];
    axios({
      method: 'get',
      url: process.env.REACT_APP_API_URL + '/dashboard/feedbacks/place',
      headers: {
        Authorization: 'Bearer ' + sessionStorage.getItem('token'),
      },
      params: {
        id: 1,
      },
    }).then(res => {
      var i = 0;
      var result = [];
      data = res.data.feedbacks;
      for (i = 0; i < data.length; i++) {
        var advice = [];
        advice.push(data[i].mark + '/5');
        advice.push(timeConverter(data[i].date));
        advice.push(data[i].body);
        result.push(advice);
      }
      const rev = result.reverse();
      this.setState({ data: rev });
    });
  }
  render() {
    return (
      <MUIDataTable title={""} data={this.state.data} columns={columns} options={options} />
    );
  }
}
