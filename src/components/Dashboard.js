import React, { Component } from 'react';
import { withStyles } from '@material-ui/core';

import '../../node_modules/react-grid-layout/css/styles.css';
import './layout/gridlayout.css';
import { WidthProvider, Responsive } from 'react-grid-layout';

import LoadTool from './LoadTool';

import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Typography from '@material-ui/core/Typography';
import ThemeContext from '../context/ThemeContext';

toast.configure();
const ResponsiveReactGridLayout = WidthProvider(Responsive);
var originalLayouts = getFromLS('layouts') || {};

const styles = theme => ({
  root: {
    padding: theme.spacing(4),
  },
});

function getFromLS(key) {
  let ls = {};
  if (global.sessionStorage) {
    try {
      ls = JSON.parse(global.sessionStorage.getItem('rgl-8')) || {};
    } catch (e) {
      //Ignore
    }
  }
  return ls[key];
}

function saveToLS(key, value) {
  if (global.sessionStorage) {
    global.sessionStorage.setItem(
      'rgl-8',
      JSON.stringify({
        [key]: value,
      })
    );
  }
}

class Dashboard extends Component {
  static contextType = ThemeContext;
  constructor(props) {
    super(props);
    this.state = {
      layouts: originalLayouts,
      myTools: [],
      customPref: false,
    };
    toast.info(
      <div style={{ backgroundColor: 'inherit', color: 'white', margin: 20 }}>
        <h4>Conseil: </h4>
        <Typography variant="subtitle2">
          Cliquez sur "Réglages" dans la barre de gauche pour activer d'autres outils!
          Cliquez sur le Gestionnaire pour gérer les plats et menus.
        </Typography>
      </div>,
      { hideProgressBar: true, position: 'bottom-right' }
    );
  }

  componentDidUpdate() {
    originalLayouts = getFromLS('layouts') || {};
  }

  static get defaultProps() {
    return {
      className: 'layout',
      isResizable: false,
      autoSize: true,
      backgroundColor: "green",
      color: "green",
      cols: { lg: 12, md: 10, sm: 6, xs: 4, xxs: 2 },
      rowHeight: 30,
    };
  }

  resetLayout() {
    this.setState({ layouts: {} });
    sessionStorage.removeItem('rgl-8');
  }

  onLayoutChange(layout, layouts) {
    saveToLS('layouts', layouts);
    this.setState({ layouts });
  }

  defaultRender() {
    return (
      <div style={{marginTop: 60}}>
        <ResponsiveReactGridLayout
          className="layout"
          isResizable={false}
          autoSize={true}
          cols={{ lg: 12, md: 10, sm: 6, xs: 4, xxs: 2 }}
          rowHeight={30}
          margin={[40, 40]}
          layouts={this.state.layouts}
          onLayoutChange={(layout, layouts) => this.onLayoutChange(layout, layouts)}
        >
          <div
            className="item"
            key="1"
            data-grid={{ w: 6, h: 8, x: 0, y: 0, minW: 2, minH: 3 }}
          >
            <LoadTool component="Graphe d'évolution" />
          </div>
          <div
            className="item"
            key="2"
            data-grid={{ w: 6, h: 4, x: 6, y: 0, minW: 2, minH: 3 }}
          >
            <LoadTool component="Plat favori" />
          </div>
          <div
            className="item"
            key="3"
            data-grid={{ w: 6, h: 4, x: 6, y: 3, minW: 2, minH: 3 }}
          >
            <LoadTool component="Plat le moins aimé" />
          </div>
          <div
            className="item"
            key="5"
            data-grid={{ w: 6, h: 5, x: 0, y: 12, minW: 5, minH: 5 }}
          >
            <LoadTool component="Statistiques de l'établissement" />
           </div>
        </ResponsiveReactGridLayout>
      </div>
    );
  }

  customRender(fromstore) {
    return (
      <div style={{marginTop: 60}}>
        <ResponsiveReactGridLayout
          className="layout"
          isResizable={false}
          autoSize={true}
          cols={{ lg: 12, md: 10, sm: 6, xs: 4, xxs: 2 }}
          rowHeight={30}
          margin={[40, 40]}
          layouts={this.state.layouts}
          onLayoutChange={(layout, layouts) => this.onLayoutChange(layout, layouts)}
        >
          {fromstore.includes('WeekChart') ? (
            <div
              className="item"
              key="1"
              data-grid={{ w: 6, h: 8, x: 0, y: 0, minW: 2, minH: 8 }}
            >
              <LoadTool component="Graphe d'évolution" />
            </div>
          ) : (
            <div></div>
          )}
          {fromstore.includes('Favourite') ? (
            <div
              className="item"
              key="2"
              data-grid={{ w: 6, h: 4, x: 6, y: 0, minW: 2, minH: 3 }}
            >
              <LoadTool component="Plat favori" />
            </div>
          ) : (
            <div></div>
          )}
          {fromstore.includes('Worst') ? (
            <div
              className="item"
              key="3"
              data-grid={{ w: 6, h: 4, x: 6, y: 3, minW: 2, minH: 3 }}
            >
              <LoadTool component="Plat le moins aimé" />
            </div>
          ) : (
            <div></div>
          )}
          {fromstore.includes('Feedback') ? (
            <div
              className="item"
              key="4"
              data-grid={{ w: 6, h: 8, x: 0, y: 0, minW: 2, minH: 8}}
            >
              <LoadTool component="Retours utilisateurs" />
            </div>
          ) : (
            <div></div>
          )}
          {fromstore.includes('ProfilTool') ? (
            <div
              className="item"
              key="5"
              data-grid={{ w: 6, h: 10, x: 0, y: 12, minW: 5, minH: 10 }}
            >
              <LoadTool component="Statistiques de l'établissement" />
            </div>
          ) : (
            <div></div>
          )}
          {fromstore.includes('TotalConso') ? (
            <div
              className="item"
              key="7"
              data-grid={{ w: 6, h: 6, x: 6, y: 3, minW: 2, minH: 6 }}
            >
              <LoadTool component="Statistiques consommateurs" />
            </div>
          ) : (
            <div></div>
          )}
          {fromstore.includes('TotalProfit') ? (
            <div
              className="item"
              key="8"
              data-grid={{ w: 6, h: 5, x: 6, y: 3, minW: 2, minH: 2 }}
            >
              <LoadTool component="Total des pertes (euros)" />
            </div>
          ) : (
            <div></div>
          )}
          {fromstore.includes('EstablishmentFeedback') ? (
            <div
              className="item"
              key="9"
              data-grid={{ w: 6, h: 8, x: 0, y: 0, minW: 2, minH: 8 }}
            >
              <LoadTool component="Retours sur l'établissement" />
            </div>
          ) : (
            <div></div>
          )}
          {fromstore.includes('Circular') ? (
            <div
              className="item"
              key="10"
              data-grid={{ w: 6, h: 8, x: 0, y: 0, minW: 2, minH: 8 }}
            >
              <LoadTool component="Pourcentage de gaspillage selon le type" />
            </div>
          ) : (
            <div></div>
          )}
        </ResponsiveReactGridLayout>
      </div>
    );
  }

  render() {
    const { classes } = this.props;
    const fromstore = JSON.parse(sessionStorage.getItem('WastepointTools'));

    return <div className={classes.root}>{fromstore ? this.customRender(fromstore) : this.defaultRender()}</div>;
  }
}

export default withStyles(styles)(Dashboard);
