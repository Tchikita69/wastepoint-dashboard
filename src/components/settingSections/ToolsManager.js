import React, { Component } from 'react';
import { Grid, Typography, Switch, Paper, ThemeProvider } from '@material-ui/core';
import { withStyles, createMuiTheme } from '@material-ui/core';

import ThemeContext from '../../context/ThemeContext';

import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import ThumbDownIcon from '@material-ui/icons/ThumbDown';
import BarChartIcon from '@material-ui/icons/BarChart';
import HappyIcon from '@material-ui/icons/SentimentSatisfied';
import EstablishmentFeedbackIcon from '@material-ui/icons/FeedbackRounded';
import SearchIcon from '@material-ui/icons/Search';
import TrendingUpIcon from '@material-ui/icons/TrendingUp';
import EuroIcon from '@material-ui/icons/Euro';
import CircularIcon from '@material-ui/icons/DataUsage';

import Slide from '@material-ui/core/Slide';

const styles = theme => ({
  root: {
    margin: 35,
  },
  paper: {
    padding: theme.spacing(2),
    width: 400,
    height: 140,
  },
});

const theme = createMuiTheme({
  overrides: {
    MuiSwitch: {
      switchBase: {
        // Controls default (unchecked) color for the thumb
        color: "#ccc"
      },
      colorSecondary: {
        "&$checked": {
          // Controls checked color for the thumb
          color: "green"
        }
      },
      track: {
        // Controls default (unchecked) color for the track
        opacity: 0.2,
        backgroundColor: "grey",
        "$checked$checked + &": {
          // Controls checked color for the track
          opacity: 0.5,
          backgroundColor: "green"
        }
      }
    }
  }
});

class ToolsManager extends Component {
  static contextType = ThemeContext;

  constructor(props) {
    super(props);
    this.state = {
      WeekChart: true,
      Favourite: true,
      Worst: true,
      Feedback: false,
      EstablishmentFeedback: false,
      ProfilTool: true,
      TotalConso: false,
      TotalProfit: false,
      Circular: false,
      ChangePDJ: false,
      ToolstoSend: [
        'WeekChart',
        'Worst',
        'Favourite',
        'ProfilTool'
      ],
    };
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    const fromstore = JSON.parse(sessionStorage.getItem('WastepointTools'));

    if (fromstore) {
      this.removeAll();
      this.setState(state => ({
        ToolstoSend: [...state.ToolstoSend, ...fromstore],
      }));
      this.setState({
        WeekChart: fromstore.includes('WeekChart'),
        Favourite: fromstore.includes('Favourite'),
        Worst: fromstore.includes('Worst'),
        Feedback: fromstore.includes('Feedback'),
        EstablishmentFeedback: fromstore.includes('EstablishmentFeedback'),
        ProfilTool: fromstore.includes('ProfilTool'),
        TotalConso: fromstore.includes('TotalConso'),
        TotalProfit: fromstore.includes('TotalProfit'),
        Circular: fromstore.includes('Circular')
      });
    }
  }

  componentDidUpdate() {
    sessionStorage.setItem('WastepointTools', JSON.stringify(this.state.ToolstoSend));
  }

  removeAll() {
    this.setState({
      ToolstoSend: this.state.ToolstoSend.filter(function (tmp) {
        return null;
      }),
    });
  }

  removeTool(tool) {
    this.setState({
      ToolstoSend: this.state.ToolstoSend.filter(function (tmp) {
        return tmp !== tool;
      }),
    });
  }

  addTool(tool) {
    this.setState(state => ({
      ToolstoSend: [...state.ToolstoSend, tool],
    }));
  }

  handleChange(e) {
    this.setState({ ...this.state, [e.target.name]: e.target.checked });
    e.target.checked ? this.addTool(e.target.name) : this.removeTool(e.target.name);
  }

  getDesc(title) {
    switch (title) {
      case 'WeekChart':
        return {
          title: <div style={{fontSize: 20}}>Graphique d'évolution</div>,
          desc: 'Taux de gaspillage sur une periode.',
          type: 'Graphe',
          icon: <BarChartIcon style={{ fontSize: 50 }} />,
        };
      case 'Favourite':
        return {
          title: <div style={{fontSize: 20}}>Plat favori</div>,
          desc: 'Plat le plus apprécié actuellement.',
          type: 'Infos',
          icon: <ThumbUpIcon style={{ fontSize: 50 }} />,
        };
      case 'Worst':
        return {
          title: <div style={{fontSize: 20}}>Plat le moins aimé</div>,
          desc: 'Plat le moins aimé actuellemment.',
          type: 'Infos',
          icon: <ThumbDownIcon style={{ fontSize: 50 }} />,
        };
      case 'Feedback':
        return {
          title: <div style={{fontSize: 20}}>Retours utilisateurs</div>,
          desc: 'Retours des utilisateurs.',
          type: 'Commentaires',
          icon: <HappyIcon style={{ fontSize: 50 }} />,
        };
      case 'EstablishmentFeedback':
        return {
          title: <div style={{fontSize: 20}}>Notes (établissement)</div>,
          desc: "Retours sur l'établissement.",
          type: 'Commentaires',
          icon: <EstablishmentFeedbackIcon style={{ fontSize: 50 }} />,
        };
      case 'ProfilTool':
        return {
          title: <div style={{fontSize: 20}}>Statistiques de l'établissement</div>,
          desc: "Informations sur l'établissement.",
          type: 'Infos',
          icon: <SearchIcon style={{ fontSize: 50 }} />,
        };
      case 'TotalConso':
        return {
          title: <div style={{fontSize: 20}}>'Statistiques sur les consommateurs'</div>,
          desc: "",
          type: 'Infos',
          icon: <TrendingUpIcon style={{ fontSize: 50 }} />,
        };
      case 'TotalProfit':
        return {
          title: <div style={{fontSize: 20}}>'Total des pertes (euros)'</div>,
          desc: "Perte d'argent dûe au gaspillage.",
          type: 'Infos',
          icon: <EuroIcon style={{ fontSize: 50 }} />,
        };
        case 'Circular':
          return {
            title: <div style={{fontSize: 20}}>'Diagramme circulaire'</div>,
            desc: "Taux de gaspillage selon le type d'aliments.",
            type: 'Infos',
            icon: <CircularIcon style={{fontSize: 50}}/>
          };

      default:
        break;
    }
  }

  handleItem(title, state) {
    const { classes } = this.props;
    const meta = this.getDesc(title);

    return (
      <ThemeProvider theme={theme}>
      <div className={classes.root}>
        <Paper className={classes.paper}>
          <Grid container spacing={2}>
            <Grid item>{meta.icon}</Grid>
            <Grid item xs={12} sm container>
              <Grid item xs container direction="column" spacing={2}>
                <Grid item xs>
                  <Typography gutterBottom variant="subtitle1">
                    {meta.title}
                  </Typography>
                  <Typography variant="body2" gutterBottom>
                    Type: {meta.type}
                  </Typography>
                  <Typography variant="body2" color="textSecondary">
                    {meta.desc}
                  </Typography>
                </Grid>
              </Grid>
              <Grid item>
                <Switch onChange={this.handleChange} checked={state} name={title} color="secondary" />
              </Grid>
            </Grid>
          </Grid>
        </Paper>
      </div>
      </ThemeProvider>
    );
  }

  render() {
    return (
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          backgroundColor: 'white',
        }}
      >
        <h2
          style={{
            color: this.context.style.buttonBackground,
            fontWeight: 'normal',
            fontSize: 35,
            marginLeft: 790,
            marginTop: 50,
          }}
        >
        </h2>
        <Slide direction="right" in={true} mountOnEnter unmountOnExit>
          <div
            style={{
              marginLeft: '200px',
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'flex-start',
              flexWrap: 'wrap',
            }}
          >
            {this.handleItem('WeekChart', this.state.WeekChart)}
            {this.handleItem('Favourite', this.state.Favourite)}
            {this.handleItem('Worst', this.state.Worst)}
            {this.handleItem('Feedback', this.state.Feedback)}
            {this.handleItem('EstablishmentFeedback', this.state.EstablishmentFeedback)}
            {this.handleItem('ProfilTool', this.state.ProfilTool)}
            {this.handleItem('TotalConso', this.state.TotalConso)}
            {this.handleItem('TotalProfit', this.state.TotalProfit)}
            {this.handleItem('Circular', this.state.Circular)}
          </div>
        </Slide>
      </div>
    );
  }
}

export default withStyles(styles)(ToolsManager);