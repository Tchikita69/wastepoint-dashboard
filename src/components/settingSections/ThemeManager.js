import React, { Component } from 'react';
import { withStyles } from '@material-ui/core';

import ThemeContext from '../../context/ThemeContext';

import Button from '@material-ui/core/Button';

import green from '../../media/greenWastePoint.png';
import red from '../../media/redRubis.png';
import blue from '../../media/bleuCiel.png';

const scale = 5;
const width = 500;
const height = 500;

const styles = theme => ({
  root: {
    margin: 35,
  },
  paper: {
    padding: theme.spacing(2),
    width: 400,
    height: 150,
  },
});

class ThemeManager extends Component {
  static contextType = ThemeContext;

  constructor(props) {
    super(props);
    this.state = {
    };
    this.handleChange = this.handleChange.bind(this);
  }
  
  render() {
    return (
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          backgroundColor: 'white',
        }}
      >
        <h2
          style={{
            color: this.context.style.h2,
            fontSize: 30,
            marginLeft: 750,
            marginTop: 50,
          }}
        >
          Choix du thème
        </h2>
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            marginLeft: 570,
            marginTop: 100,
            flexWrap: 'wrap',
          }}
        >
          <Button onClick={() => this.context.toggle('green')}>
            <img
              style={{ borderRadius: 70 }}
              src={green}
              width={(width / scale) * 1.2}
              height={(height / scale) * 1.2}
              alt="green"
            />
          </Button>
          <Button style={{ marginLeft: 100 }} onClick={() => this.context.toggle('red')}>
            <img
              style={{ borderRadius: 70 }}
              src={red}
              width={(width / scale) * 1.2}
              height={(height / scale) * 1.2}
              alt="green"
            />
          </Button>
          <Button style={{ marginLeft: 100 }} onClick={() => this.context.toggle('blue')}>
            <img
              style={{ borderRadius: 70 }}
              src={blue}
              width={(width / scale) * 1.2}
              height={(height / scale) * 1.2}
              alt="blue"
            />
          </Button>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(ThemeManager);
