import React, { Component } from 'react';

import WeekChart2 from './Tools/WeekChart2';
import Circular from './Tools/Circular';
import Worst from './Tools/Worst';
import Favourite from './Tools/Favourite';
import Feedback from './Tools/Feedback';
import EstablishmentFeedback from './Tools/EstablishmentFeedback';
import ProfilTool from './Tools/ProfilTool';
import TotalConso from './Tools/TotalConso';
import TotalProfit from './Tools/TotalProfit';

import './layout/gridlayout.css';

import ThemeContext from '../context/ThemeContext';

export default class LoadTool extends Component {
  static contextType = ThemeContext;

  loadTool(component) {
    switch (component) {
      case "Pourcentage de gaspillage selon le type":
        return <Circular />;
      case "Graphe d'évolution":
        return <WeekChart2 />;
      case 'Plat le moins aimé':
        return <Worst />;
      case 'Plat favori':
        return <Favourite />;
      case 'Retours utilisateurs':
        return <Feedback />;
      case "Retours sur l'établissement":
        return <EstablishmentFeedback />;
      case "Statistiques de l'établissement":
        return <ProfilTool />;
      case 'Statistiques consommateurs':
        return <TotalConso />;
      case 'Total des pertes (euros)':
        return <TotalProfit />;
      default: {
        return <p style={{ marginLeft: 20 }}>Erreur: Impossible de charger l'outil</p>;
      }
    }
  }

  render() {
    const { component } = this.props;

    return (
      <div>
        <div className="MyDragHandleClassName">
          <span className="text" style={{fontSize: 20}}>{component}</span>
        </div>
        <div className="content">{this.loadTool(component)}</div>
      </div>
    );
  }
}
