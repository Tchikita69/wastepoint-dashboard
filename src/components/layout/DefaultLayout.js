import React, { useContext } from 'react';
import clsx from 'clsx';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { Link } from 'react-router-dom';
import BuildIcon from '@material-ui/icons/Build';
import HomeIcon from '@material-ui/icons/Home';
import InfoIcon from '@material-ui/icons/Info';
import SmileIcon from '@material-ui/icons/Face';
import AccountIcon from '@material-ui/icons/AccountCircle';
import NotificationsNoneIcon from '@material-ui/icons/NotificationsNone';
import Questions from '@material-ui/icons/QuestionAnswer';
import MeetingRoomIcon from '@material-ui/icons/MeetingRoom';
import Grid from '@material-ui/core/Grid';
import logo from '../../media/wastepoint-txt-white.png';
import ThemeContext from '../../context/ThemeContext';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import ThemeMenu from './SimpleMenu';

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    height: '100%',
    width: '100%'
  },
  appBar: {
    color: "white",
    flexGrow: 1,
    zIndex: theme.zIndex.drawer + 1,
    backgroundColor: 'white',
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: '100%',
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  link: {
    textDecoration: 'none',
    color: 'grey',
  },
  menuButton: {
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9) + 1,
    },
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(1),
    textAlign: "center",
    color: theme.palette.text.secondary
  }
}));

export default function MiniDrawer({ children }) {
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);
  const [openQuit, setOpenQuit] = React.useState(false);
  const scale = 8;
  const width = 1508;
  const height = 347;

  const { style } = useContext(ThemeContext);

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const handleQuitOpen = () => {
    setOpenQuit(true);
  };

  const handleQuitClose = () => {
    setOpenQuit(false);
  };

  const byeCredentials = () => {
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('user_id');
  }

  return (
    <div className={classes.root}>
      <CssBaseline />
      <Drawer
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}
      >
        <div className={classes.toolbar} style={{ marginTop: 20 }}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
          </IconButton>
        </div>
        <div>
          <List>
            <ListItem style={{width: 70 }}>
            <Typography style={{ fontSize: 10, color: 'white' }}>T</Typography>
            </ListItem>
            <ListItem style={{width: 70 }}>
                <ListItemText primary={<ThemeMenu/>} />
            </ListItem>
            <Link to="/mytools" className={classes.link}>
              <ListItem button>
                <ListItemIcon style={{marginLeft: 5}}>
                  <BuildIcon />
                </ListItemIcon>
              </ListItem>
            </Link>
            <Divider style={{ marginBottom: 20 }} variant="inset" />
            <Link className={classes.link}>
              <ListItem button onClick={handleQuitOpen}>
                <ListItemIcon style={{marginLeft: 5}}>
                  <MeetingRoomIcon />
                </ListItemIcon>
                <ListItemText primary={'Se déconnecter'} />
              </ListItem>
            </Link>
          </List>
        </div>
      </Drawer>
      <AppBar
        style={{ backgroundColor: style.appBarBackground }}
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar>
          <Grid container spacing={1} direction="column">
          <Grid item style={{ marginLeft: 880, paddingBottom: 0}}>
              <Grid container spacing={2} direction="row">
                <Grid item>
                  <Link to="/" className={classes.link}>
                  <img src={logo} alt="Logo-wastepoint" width={width / scale} height={height / scale} />
                  </Link>
                </Grid>
              </Grid>
            </Grid>
            <Grid item>
              <Grid container spacing={1} direction="row" style={{marginLeft:630}}>
              <Grid item>
                <Link to="/home" className={classes.link}>
                  <ListItem button style={{marginTop:0, width: 160 }}>
                  <ListItemText primary={<Typography style={{ color: 'white' }}>Tableau de bord</Typography>} />
                  </ListItem>
                </Link>
                </Grid>
                <Grid item>
                <Link to="/profil" className={classes.link}>
                  <ListItem button style={{marginTop:0, width: 130 }}>
                  <ListItemText primary={<Typography style={{ color: 'white' }}>Gestionnaire</Typography>} />
                  </ListItem>
                </Link>
                </Grid>
                <Grid item>
                <Link to="/aboutus" className={classes.link}>
                  <ListItem button style={{marginTop:0, width: 180 }}>
                  <ListItemText primary={<Typography style={{ color: 'white' }}>A propos de nous</Typography>} />
                  </ListItem>
                </Link>
                </Grid>
                <Grid item>
                <Link to="/faq" className={classes.link}>
                  <ListItem button style={{marginTop:0, width: 65 }}>
                  <ListItemText primary={<Typography style={{ color: 'white' }}>FAQ</Typography>} />
                  </ListItem>
                </Link>
                </Grid>
                <Grid item>
                <Link to="/contact" className={classes.link}>
                  <ListItem button style={{marginTop:0, width: 95 }}>
                  <ListItemText primary={<Typography style={{ color: 'white' }}>Contact</Typography>} />
                  </ListItem>
                </Link>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
      
      <div className={classes.content}>
      
        <div className={classes.toolbar} />
        {children}
        </div>
      
        <div>
        <Dialog open={openQuit} onClose={handleQuitClose} maxWidth={'sm'} fullWidth={true}>
          <DialogTitle>Déconnexion</DialogTitle>
          <DialogContent>
            <DialogContentText>Vous allez être déconnecté(e).</DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button autoFocus onClick={handleQuitClose} color="primary">
              Annuler
            </Button>
            <Link to="/auth" onClick={byeCredentials} className={classes.link}>
              <Button onClick={handleQuitClose} color="primary">
                OK
              </Button>
            </Link>
          </DialogActions>
        </Dialog>
        </div>
    </div>
  );
}



