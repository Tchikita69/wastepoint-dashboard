import React, { useCallback } from "react";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import Typography from '@material-ui/core/Typography';
import green from '../../media/greenWastePoint.png';
import red from '../../media/redRubis.png';
import blue from '../../media/bleuCiel.png';

import ThemeContext from '../../context/ThemeContext';


function SimpleMenu() {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [colorTheme, setColorTheme] = React.useState('green');
  const context = React.useContext(ThemeContext);
  function handleClick(event) {
    if (anchorEl !== event.currentTarget) {
      setAnchorEl(event.currentTarget);
    }
  }

  function handleClose() {
    setAnchorEl(null);
  }

  return (
    <div>
      <div
        aria-owns={anchorEl ? "simple-menu" : undefined}
        aria-haspopup="true"
        onClick={handleClick}
        onMouseOver={handleClick}
      >
      {<Typography style={{ fontSize: 10, color: 'grey' }}>Thème</Typography>}
      <img
              style={{ borderRadius: 70, marginLeft: 6 }}
              src={(colorTheme == "green" ? green : colorTheme == "red" ? red : blue)}
              width={20}
              height={20}
              alt={colorTheme}
            />
      </div>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={handleClose}
        MenuListProps={{ onMouseLeave: handleClose }}
        getContentAnchorEl={null}
      anchorOrigin={{vertical: 'bottom', horizontal: 'left'}}
      >
        <MenuItem onClick={() => {context.toggle('green'); setColorTheme("green");}}>
            <img
              style={{ borderRadius: 70 }}
              src={green}
              width={10}
              height={10}
              alt="green"
            />
        </MenuItem>
        <MenuItem onClick={() => {context.toggle('red'); setColorTheme("red");}}>
            <img
              style={{ borderRadius: 70 }}
              src={red}
              width={10}
              height={10}
              alt="red"
            />
        </MenuItem>
        <MenuItem onClick={() => {context.toggle('blue'); setColorTheme("blue");}}>
            <img
              style={{ borderRadius: 70 }}
              src={blue}
              width={10}
              height={10}
              alt="blue"
            />
        </MenuItem>
      </Menu>
    </div>
  );
}

export default SimpleMenu;