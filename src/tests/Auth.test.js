import React from 'react';
import Authentification from '../components/Authentification';
import FirstConnection from '../components/FirstConnection';
import { mount } from 'enzyme';
import './setupTests';

describe('<Auth>', () => {
  const wrapper = mount(<Authentification />);
  const button = wrapper.find('#connection_btn');

  it('renders the connection button', () => {
    expect(button).toHaveLength(1);
  });

  it("doesn't connect when username or passwords are empty", () => {
    button.simulate('click');
    expect(wrapper.find(FirstConnection).length).toEqual(0);
  });

  it('connect when username or passwords', () => {
    wrapper.setState({ user_id: 'test', password: 'test' });
    button.simulate('click');
    expect(wrapper.find(FirstConnection).length).toEqual(1);
  });
});
