import React from 'react';
import LoadTool from '../components/LoadTool';

// My tools
import Favourite from '../components/Tools/Favourite';
import Feedback from '../components/Tools/Feedback';
import SearchBar from '../components/Tools/SearchBar';
import WeekChart from '../components/Tools/WeekChart2';
import Worst from '../components/Tools/Worst';

import { shallow } from 'enzyme';
import './setupTests';

describe('<LoadTool> tests', () => {
  it('Weekchart', () => {
    const wrapper = shallow(<LoadTool component="Graphe d'évolution" />);
    expect(wrapper.find(WeekChart).length).toEqual(1);

    expect(wrapper.find(Favourite).length).toEqual(0);
    expect(wrapper.find(Worst).length).toEqual(0);
    expect(wrapper.find(Feedback).length).toEqual(0);
    expect(wrapper.find(SearchBar).length).toEqual(0);
  });

  it('Favourite', () => {
    const wrapper = shallow(<LoadTool component="Plat favori" />);
    expect(wrapper.find(Favourite).length).toEqual(1);

    expect(wrapper.find(WeekChart).length).toEqual(0);
    expect(wrapper.find(Worst).length).toEqual(0);
    expect(wrapper.find(Feedback).length).toEqual(0);
    expect(wrapper.find(SearchBar).length).toEqual(0);
  });

  it('Worst', () => {
    const wrapper = shallow(<LoadTool component="Plat le moins aimé" />);
    expect(wrapper.find(Worst).length).toEqual(1);

    expect(wrapper.find(WeekChart).length).toEqual(0);
    expect(wrapper.find(Favourite).length).toEqual(0);
    expect(wrapper.find(Feedback).length).toEqual(0);
    expect(wrapper.find(SearchBar).length).toEqual(0);
  });

  it('Feedback', () => {
    const wrapper = shallow(<LoadTool component="Retours utilisateurs" />);
    expect(wrapper.find(Feedback).length).toEqual(1);

    expect(wrapper.find(WeekChart).length).toEqual(0);
    expect(wrapper.find(Favourite).length).toEqual(0);
    expect(wrapper.find(Worst).length).toEqual(0);
    expect(wrapper.find(SearchBar).length).toEqual(0);
  });

  it('SearchBar', () => {
    const wrapper = shallow(<LoadTool component="Outil de recherche" />);
    expect(wrapper.find(SearchBar).length).toEqual(1);

    expect(wrapper.find(WeekChart).length).toEqual(0);
    expect(wrapper.find(Favourite).length).toEqual(0);
    expect(wrapper.find(Feedback).length).toEqual(0);
    expect(wrapper.find(Worst).length).toEqual(0);
  });

  it('Unknown tool', () => {
    const wrapper = shallow(<LoadTool component="Unknown tool" />);

    expect(wrapper.find(SearchBar).length).toEqual(0);
    expect(wrapper.find(WeekChart).length).toEqual(0);
    expect(wrapper.find(Favourite).length).toEqual(0);
    expect(wrapper.find(Feedback).length).toEqual(0);
    expect(wrapper.find(Worst).length).toEqual(0);
  });
});
