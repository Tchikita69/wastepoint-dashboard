export const styles = theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  avatar: {
    margin: theme.spacing(1),
    color: 'white',
    backgroundColor: '#32A93F',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
    color: 'white',
  },
  submit: {
    margin: theme.spacing(3, 6, 2),
    color: 'white',
    backgroundColor: 'green',
    fontSize: '15px',
    fontWeight: 600,
  },
  button: {
    margin: theme.spacing(3, 8, 2),
    fontSize: '15px',
    fontWeight: 600,
  },
  testbutton: {
    margin: theme.spacing(3, 6, 2),
    fontSize: '15px',
    fontWeight: 600,
  },
  text: {
    color: 'white',
    fontSize: '15px',
    fontWeight: 600,
  },
  input: {
    color: 'black',
    fontSize: '15px',
  },
  copyright: {
    color: 'white',
    fontSize: '13px',
  },
});
