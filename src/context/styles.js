export const greenStyle = {
  appBackground: 'white',
  paperBackground: 'white',
  appBarBackground: '#02c853',
  buttonBackground: '#02c853',
  h2: '#0d7e13',
  fontColor: 'white',
};

//#d4f9d6
//#73e879

export const redStyle = {
  appBackground: 'white',
  paperBackground: 'white',
  appBarBackground: '#af080d',
  buttonBackground: '#af080d',
  h2: '#780218',
  fontColor: 'white',
};

export const blueStyle = {
  appBackground: 'white',
  paperBackground: 'white',
  appBarBackground: '#00b9ff',
  buttonBackground: '#00b9ff',
  h2: '#021978',
  fontColor: 'white',
};
