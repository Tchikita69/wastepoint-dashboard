import React, { useState, useLayoutEffect, createContext } from 'react';
import { redStyle, blueStyle, greenStyle } from './styles';

const ThemeContext = createContext({
  color: '',
  toggle: () => {},
  style: {},
});

export default ThemeContext;

export function ThemeProvider(props) {
  // keeps state of the current theme
  const [color, setColor] = useState('green');
  const [style, setStyle] = useState('greenStyle');

  // paints the app before it renders elements
  useLayoutEffect(() => {
    const theme = window.sessionStorage.getItem('theme');
    setColor(theme);
    if (theme === 'green') {
      setStyle(greenStyle);
    } else if (theme === 'red') {
      setStyle(redStyle);
    } else if (theme === 'blue') {
      setStyle(blueStyle);
    }
    // if state changes, repaints the app
  }, [color]);

  const toggle = color => {
    setColor(color);
    window.sessionStorage.setItem('theme', color);
  };

  return (
    <ThemeContext.Provider
      value={{
        color,
        toggle,
        style,
      }}
    >
      {props.children}
    </ThemeContext.Provider>
  );
}
